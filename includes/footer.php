<footer id="footer" class="top-space">
  <?php include 'compteur_visite.php' ; ?>
		<div class="footer1" style="background-color:#232323">
			<div class="container">
				<div class="row" style="margin-right:15px">
					
					<div class="col-md-3 widget">
						<h3 class="widget-title">Contact</h3>
						<div class="widget-body">
							<p>+216 24 563 458<br>
								<a href="mailto:#">mediterranneenne@gmail.com</a><br>
								<br>Rue de l'environnement-Route Eljem-Immeuble Salah Elmakki, Souassi-Mahdia, TN 5140
								
							</p>	
						</div>
					</div>

					<div class="col-md-3 widget">
						<h3 class="widget-title">Suivez-nous</h3>
						<div class="widget-body">
							<p class="follow-me-icons">
		               <a href="https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fwww.fnmaker.com%2Ftwitterforpage%2Ftab%2F&ref_src=twsrc%5Etfw&screen_name=societesms&tw_p=followbutton"><i class="fa fa-twitter fa-2"></i></a>
				<a href=""><i class="fa fa-dribbble fa-2"></i></a>
				<a href=""><i class="fa fa-github fa-2"></i></a>
				<a href="https://web.facebook.com/smsfm.tn/?fref=ts"><i class="fa fa-facebook fa-2"></i></a>
							</p>	
						</div>
					</div>

					<div class="col-md-6 widget">
						<h3 class="widget-title">SMS FM</h3>
						<div class="widget-body">
							<p>SMS FM est un radio constitué par un jeune équipe, motivée et bénévole. Il est orienté vers un public jeune. Nous varions au maximum les sujets abordés. Nous offrons à nos auditeurs une programmation cohérente et fluide.</p>
							<p>Notre radio au service de la population avec notamment ses petites annonces les messages associatifs, et les informations de la vie locale.</p>
							<p>Notre équipe de la Radio contribue au renforcement de la radio de service public en assurant l'échange et la distribution des informations.</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

		<div class="footer2" style="background-color:#191919">
			<div class="container">
				<div class="row" style="margin-right:15px">
					
					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="simplenav">
								<a href="index.php">Acceuil</a> | 
								<a href="page_actualite.php">Actualité</a> |
								<a href="page_music.php">Music</a> |
								<a href="index.php">Podcast</a> |
								<a href="page_images.php">Galerie</a>
								<b><a href="page_contact.php">Contact</a></b>
							</p>
						</div>
					</div>

					<div class="col-md-6 widget">
						<div class="widget-body">
							<p class="text-right">
								Copyright &copy; All Rights Reserved.2016, <b> SMS FM  <b>
							</p>
						</div>
					</div>

				</div> <!-- /row of widgets -->
			</div>
		</div>

	</footer>
	
	<script type='text/javascript' src='scrollToTop.min.js'></script>
<script type='text/javascript'>
	jQuery(document).ready(function($){
		$('#wrapper').backtotop({
			topOffset: 500,
			animationSpeed: 2000,
			bckTopLinkTitle: ' ↑ Haut'
		});	
	});
</script>