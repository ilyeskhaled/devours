<!DOCTYPE html>

<html>

   <head>

      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="image/icon_sms.png" type="image/x-icon">
      <title>Music</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


     


  

   

   </head>

   <body> 
   

<div id="wrapper">
	<div id="page-wrapper">

		<div class="container-fluid">
      

			<?php include('includes/headerM.php'); ?>

        </div>
		<div class="row">
 <div class="col-lg-12">
 <div class="panel panel-primary">
  
 <div class="panel-heading" >
 <h3 class="panel-title">TOP MUSIC</h3>
 </div>
<div class="panel-body">
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c;">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 100px;font-family: cursive;">TOP Oriental</h3>
		</div>
		</div>
		<?php require_once 'top_oriental_all.php' ?>
	
	</div>
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c;">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 100px;font-family: cursive;">TOP Occidental</h3>
	</div>
	</div>
        <?php include 'top_occidental_all.php' ?>
		
	</div>
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 100px;font-family: cursive;">TOP Occidental</h3>
		</div>
	</div>
        <?php require_once 'top_mix_all.php' ?>
		
    </div>

 </div>
 
 </div> 
			
<?php require_once 'includes/footer.php' ?>
</div>
</div>

 <script src="js/jquery.js"></script>
      <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>