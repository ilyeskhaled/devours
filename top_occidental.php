<!DOCTYPE html>
<html>

  <head>
    <title>TOP Oriental</title>
    <link rel="stylesheet" type="text/css" href="resources_occ/css/main.min.css">
  </head>


  <body>
<?php 
include 'music_occ.php';
?>
    <div class='jAudio1'>

      <audio></audio>

      <div class='jAudio1--ui'>

        <div class='jAudio1--thumb'></div>

        <div class='jAudio1--status-bar'>

          <div class='jAudio1--details'></div>
          <div class='jAudio--volume-bar'></div>

          <div class='jAudio1--progress-bar'>
            <div class='jAudio1--progress-bar-wrapper'>
              <div class='jAudio1--progress-bar-played'>
                <span class='jAudio1--progress-bar-pointer'></span>
              </div>
            </div>
          </div>

          <div class='jAudio1--time'>
            <span class='jAudio1--time-elapsed'>00:00</span>
            <span class='jAudio1--time-total'>00:00</span>
          </div>

        </div>

      </div>

      <div class='jAudio1--controls'>
        <ul>
          <li><button class='jAudio1--control jAudio1--control-prev' data-action='prev'><span></span></button></li>
          <li><button class='jAudio1--control jAudio1--control-play' data-action='play'><span></span></button></li>
          <li><button class='jAudio1--control jAudio1--control-next' data-action='next'><span></span></button></li>
        </ul>
      </div>
      <div class='jAudio1--playlist'>
      </div>

      
    </div>

    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <script src='resources_occ/js/jaudio.min.js'></script>
	
    <script type="text/javascript">var jOriental =<?php echo json_encode($occidental); ?>;</script>
    <script type="text/javascript" src="resources_occ/js/main.js"></script>

  </body>


</html>