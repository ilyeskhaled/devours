<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="author" content="Script Tutorials" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>HTML5 Audio player with playlist | Script Tutorials</title>

    <!-- add styles and scripts -->
    <link href="css/styles.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>    
    <script type="text/javascript" src="js/musicplayer.js"></script>
</head>
<body>
<?php include 'list_podcast.php'; ?>
    <div class="example" style="width: 100%">

        <ul class="playlist">
		<?php for($i=0;$i<count($podcast);$i++){
			$url= "data/". $podcast[$i]['url'];
		 ?>
		<li><a href="<?= $url ?>"><?= $podcast[$i]['titre'] ?></a></li>
		<?php } ?>
            
            
        </ul>

    </div>

     <script>
   // controls: ['play', 'rewind' , 'forward', 'stop', 'volume' , 'playlist-toggle', 'progress' ]
   //playerDisplays: ['song-title', 'artist', 'cover', 'time-durtion' ]
        $(".example").musicPlayer({
            elements: ['artwork', 'information', 'controls', 'progress', 'time', 'volume'], // ==> This will display in  the order it is inserted
            //elements: [ 'controls' , 'information', 'artwork', 'progress', 'time', 'volume' ],
            //controlElements: ['backward', 'play', 'forward', 'stop'],
            //timeElements: ['current', 'duration'],
           //timeSeparator: " : ", // ==> Only used if two elements in timeElements option
            //infoElements: [  'title', 'artist' ],  

            //volume: 10,
            //autoPlay: true,
            //loop: true,
         
           


        });

  


    </script>

   
</body>
</html>

