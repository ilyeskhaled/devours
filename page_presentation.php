<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="icon" href="image/icon_sms.png" type="image/x-icon">
    <title>Présentation</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	 <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script> 
	 <script type="text/javascript" src="js/bootstrap.min.js"></script>  
	 <script type="text/javascript" src="js/plugins.js"></script>   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	 <div id="myModal" class="modal fade">  
		<div class="modal-dialog">    
		 <div class="modal-content">  
			<div class="row">				  
				<div class="col-lg-12">	
					<div class="panel panel-primary">  
						<div class="panel-heading">
							<h3 class="panel-title"><b>Qui somme nous?</b></h3>
						</div>       
					</div>            
					<div class="panel-body">                              
						 <p>
						  <a class="facebook-action-connect" rel="nofollow" href="https://www.facebook.com/v2.3/dialog/oauth?client_id=462709097101928&amp;redirect_uri=http%3A//villedesayada.tn/fr/fboauth/connect&amp;scope=email">	
						  <img align="right" src="image/aime.jpg" width="80" alt="">	
						  </a>                
						  <img src="image/sf1.jpg" alt="..." class="img-responsive" align="center">		
						  </p>   <p style="margin-top:10px;"><br>				
						  <b>SMS FM </b>est un radio constitué par un jeune équipe, motivée et bénévole. Il est orienté vers un public jeune.  Nous varions au maximum les sujets abordés. Nous offrons à nos auditeurs une programmation cohérente et fluide.  Notre radio au service de la population avec notamment ses petites annonces les messages associatifs, et les informations de la vie locale.Notre radio sera au cœur des actualités de la région.<br><br>                             <b>La Société Méditerranéenne de Services(SMS)</b> est une Société à Responsabilité Limitée (SARL) juridiquement constituée en novembre 2008.                            <I>La Société Méditerranéenne De Services(SMS)</I> est un cabinet de formation et services et plus généralement  toutes opérations 						   de quelques natures qu’elles soient, industrielles, commerciales,						   financières et immobilières  pouvant  se rattacher   directement  ou indirectement  à l’objet social  et à tout objet similaire ou connexe. <br>						   						   <br>						   <b>Pour vous inscrire </b>: Société SMS Rue de la Liberté Souassi/Mahdia <br><b>GSM:</b> 24563458<br><b>Site:</b> ww.societesms.com<br><b>Email:</b> mediterranneenne@gmail.com.  
						  </p>      
					</div>  
				</div>   
			</div>    
		</div>   
	    </div>   
	 </div>	
	<div id="myModal0" class="modal fade"> 
		<div class="modal-dialog">     
			<div class="modal-content">    
				<div class="col-lg-12">        
					<div class="panel panel-primary"> 
						<div class="panel-heading">       
							<h3 class="panel-title"><b>L’objectif de SMS FM </b></h3>  
						</div>                  
						<div class="panel-body"> 
						  <img src="image/logo.png" alt="..." class="img-responsive"><br>    
						  <p style="margin-top:10px;">               
						  L’objectif de SMS FM est :<br><br><b>•</b>&nbsp;&nbsp;&nbsp;	La volonté de produire et diffuser l'information locale la plus complète possible.<br><br><b>•</b>&nbsp;&nbsp;&nbsp;	Donner la parole aux différents acteurs : associations, citoyens, gestionnaires publics et privés, porteurs de projets.<br><br><b>•</b>&nbsp;&nbsp;&nbsp;	Valoriser créateurs et diffuseurs culturels.<br><br><b>•</b>&nbsp;&nbsp;&nbsp;	Participer ainsi activement au développement local.<br><br><b>•</b>&nbsp;&nbsp;&nbsp;	Contribuer au développement social, culturel et économique de sa zone, en prise avec la population, en partenariat avec les associations, les collectivités locales, et les acteurs de la vie locale.<br><br><b>SMS FM</b> souhaite la bienvenue aux auditeurs de cette jeune radio                          
						  </p>                     
						</div>                
					</div>        
				</div> 
			</div>     
		</div>    
    </div>		

	<div id="myModal2" class="modal fade">  
	     <div class="modal-dialog">       
			<div class="modal-content">   
				<div class="col-lg-12">       
					<div class="panel panel-primary">     
						<div class="panel-heading">          
							<h3 class="panel-title"><b>La philosophie de notre radio</b></h3>        
						</div>                  
					<div class="panel-body"> 
						<img src="image/rad.jpg" alt="..." class="img-responsive">	
						<p style="margin-top:10px;">  <br>              
						<b>1.</b>&nbsp;&nbsp;&nbsp; Ecouter <br><b>2.</b>&nbsp;&nbsp;&nbsp; Animer<br><b>3.</b>&nbsp;&nbsp;&nbsp; Interagir <br>  <b>4.</b>&nbsp;&nbsp;&nbsp; Distribuer<br><br><b>SMS FM</b> propose aux auditeurs de la radio des programmes de qualité : radios libres, interviews exclusives etc.<br> <br>La station se veut également proche de ses auditeurs, c’est pourquoi elle est à l’écoute de tous.<br><br>Sur ce site les auditeurs peuvent interagir entre eux et avec l’équipe, tel un réseau social, en plus d’écouter la radio.<br> 
						</p>                                                   
					</div>                
					</div>      
				</div>                     
			</div>       
		</div>   
	</div>
		
	 <div id="myModal3" class="modal fade"> 
	     <div class="modal-dialog">         
	         <div class="modal-content">       
				<div class="col-lg-12">           
					<div class="panel panel-primary">  
						<div class="panel-heading">         
							<h3 class="panel-title"><b>Les Services de SMS FM </b></h3>    
						</div>                     
						<div class="panel-body">   
							<img src="image/radio.jpg" alt="..." class="img-responsive">  
							<p style="margin-top:10px;"><br><b>La première radio locale.  </b><br>

							Notre équipe de la Radio contribue au renforcement de la radio de service public en assurant l'échange et la distribution des informations.<br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Développement d'une radio associative locale à souassi.  <br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Initiation aux compétences impliquées dans l'animation d'une radio locale.<br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Une station de radio comme un outil de formation destiné aux jeunes. <br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Une radio locale à votre service. manifestations socioculturelles. <br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Votre association organise un évènement alors contactez la rédaction SMSFM.<br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Écoutez votre radio locale et régionale en direct sur www.smsfm.tn <br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	       Régie web </b><br>
							 Envie de communiquer sur le site web de SM FM.<br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;      Les petites annonces </b><br>
							Contactez nous au : 24 563 458<br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	       Régie publicitaire </b><br>
							   SMS FM propose une location d'un espace publicitaire.<br>
							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	  Associations </b><br>
							  Envoyez nous vos manifestations et évènements.<br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	  Partenariat </b><br>
							  Radio SMS FM est partenaire de vos évènements.<br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 Le standard administratif </b><br>
							 Vous souhaitez contacter l'administration.<br>

							<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	 Promotion </b><br>
							 Offrez des cadeaux aux auditeurs de Radio SMS FM.<br>
													   </p>                
						</div>                 
					 </div>           
			    </div>           
		    </div>        
	    </div>    
    </div>
                        
    <div id="myModal4" class="modal fade">
         <div class="modal-dialog"> 
			 <div class="modal-content">
				 <div class="col-lg-12"> 
					<div class="panel panel-primary">
						<div class="panel-heading">    
							<h3 class="panel-title"><b></b></h3> 
						</div>                     
						<div class="panel-body">   
							<img src="" alt="" class="img-responsive">  
							<p style="margin-top:10px;">       
							</p>                     
						</div>                
					</div>          
				 </div>        
			 </div>      
		</div>   
	</div> 
<div id="wrapper">

       
       
 <div id="page-wrapper">

            <div class="container-fluid">
             <?php include('includes/headerP.php'); ?>
            </div>
<div class="panel panel-primary">                 <div class="panel-heading"><h3 class="panel-title"><b>Qui somme nous?</b></h3></div>                <div class="panel-body">                  <div class="row">                      <div class="col-lg-4">                          <img src="image/sf.jpg" alt="..." class="img-responsive">                      </div>                      <div class="col-lg-8">					                             <h3 class="panel-title" style="color:#31B0D5;"><b>SMS FM </b></h3><br>                           <p>                              <b>SMS FM </b>est un radio constitué par un jeune équipe, motivée et bénévole. Il est orienté vers un public jeune.  Nous varions au maximum les sujets abordés. Nous offrons à nos auditeurs une programmation cohérente et fluide.  Notre radio au service de la population avec notamment ses petites annonces les messages associatifs, et les informations de la vie locale...                           </p>                           <div class="form-group pull-right" >                              <a href="#myModal" data-toggle="modal"><button class="btn btn-primary" name="login" type="submit" id="submit"><i class="glyphicon glyphicon-plus"></i></button></a>                           </div>                      </div>                  </div>                </div>            </div>

<div class="panel panel-primary">                                <div class="panel-body">                  <div class="row">                      <div class="col-lg-4">                          <img src="image/logo.png" alt="..." class="img-responsive">                      </div>                      <div class="col-lg-8">                                                     <h3 class="panel-title" style="color:#31B0D5;"><b>L’objectif de SMS FM </b></h3><br>                           <p>                            L’objectif de SMS FM est : <br><b>•</b>&nbsp;&nbsp;&nbsp;	La volonté de produire et diffuser l'information locale la plus complète possible.<br><b>•</b>&nbsp;&nbsp;&nbsp;	Donner la parole aux différents acteurs : associations, citoyens, <br>gestionnaires publics et privés, porteurs de projets.<br><b>•</b>&nbsp;&nbsp;&nbsp;	Valoriser créateurs et diffuseurs culturels.<br><b>•</b>&nbsp;&nbsp;&nbsp;	Participer ainsi activement au développement local.<br><b>•</b>&nbsp;&nbsp;&nbsp;	Contribuer au développement social, culturel et économique de sa zone,....                                                       </p>                           <div class="form-group pull-right" >                              <a href="#myModal0" data-toggle="modal"><button class="btn btn-primary" name="login" type="submit" id="submit"><i class="glyphicon glyphicon-plus"></i></button></a>                           </div>                      </div>                 
 </div>              
   </div>        
       </div>

<div class="panel panel-primary">                                <div class="panel-body">                  <div class="row">                      <div class="col-lg-4">                          <img src="image/rad.jpg" alt="..." class="img-responsive">                      </div>                      <div class="col-lg-8">                                                     <h3 class="panel-title" style="color:#31B0D5;"><b>La philosophie de notre radio</b></h3><br>                           <p>                             <b>La Société SMS</b> est managée par de jeunes cadres dynamiques jouissant d’un fort capital de confiance 							 et d’expertise dans le domaine de publicité en vous s’assurant es services suivants : <br>1.	Réponse aux appels d’offres<br>2.	Etude, Conception et développement des logiciels et des Applications Web<br>3.	Encadrement, Suivi et Gestion technique des projets informatiques<b>....</b><br>                                                       </p>                           <div class="form-group pull-right" >                              <a href="#myModal2" data-toggle="modal"><button class="btn btn-primary" name="login" type="submit" id="submit"><i class="glyphicon glyphicon-plus"></i></button></a>                           </div>                      </div>                  </div>                </div>            </div>            

<div class="panel panel-primary">                                <div class="panel-body">                  <div class="row">                      <div class="col-lg-4">                          <img src="image/radio.jpg" alt="..." class="img-responsive">                      </div>                      <div class="col-lg-8">                                                     <h3 class="panel-title" style="color:#31B0D5;"><b>Les services de SMS FM</b></h3>                           
<p><br>
<b>La première radio locale.  </b><br>

                        Notre équipe de la Radio contribue au renforcement de la radio de service public en assurant l'échange et la distribution des informations.<br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Développement d'une radio associative locale à souassi.  <br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Initiation aux compétences impliquées dans l'animation d'une radio locale.<br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Une station de radio comme un outil de formation destiné aux jeunes. <br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Une radio locale à votre service. manifestations socioculturelles. <br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Votre association organise un évènement alors contactez la rédaction SMSFM.<br>
<b>•&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>Écoutez votre radio locale et régionale en direct sur <u>www.smsfm.tn </u><b>....</b><br>
                           </p>
                           <div class="form-group pull-right" >

                              <a href="#myModal3" data-toggle="modal"><button class="btn btn-primary" name="login" type="submit" id="submit"><i class="glyphicon glyphicon-plus"></i></button></a>

                           </div>                                              
 </div>                 
  </div>            
      </div>        
          </div>   
				
                
               

         
            <!-- /.container-fluid -->
<?php include 'includes/footer.php' ?>

        </div>
        <!-- /#page-wrapper -->

    
	
    <!-- /#wrapper -->

  
</body>

</html>
