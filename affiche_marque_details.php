<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Actualité</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper">

       
       
 <div id="page-wrapper">

            <div class="container-fluid">
             <?php include('includes/header.php'); ?>
            </div>
<div class="row">
    <div class="col-lg-3">
	<div class="panel panel-primary" style="height:441px">
		<div class="panel-heading">
			<h3 class="panel-title">Podcast</h3>
		</div>
		<div class="panel-body">

                            
<?php include 'podcast.php'; ?>

		</div>
		<a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">View Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
        </a>
		
	</div>
	</div>
	 <div class="col-lg-6" style="width:52.55%;margin-left:-15px;margin-right:15px;">
	 <div class="panel panel-primary" style="height:803px">
		<div class="panel-heading">
			<h3 class="panel-title">Actualité</h3>
		</div>
		<div class="panel-body">
	 <?php 
 $marque = $_SESSION['tableau'];
 $type = $marque[4];
 $lien = $marque[3];
?>
         <div class="page-header" >
                    <h4 style="color:#000"><?= $marque[0] ?></h4>
					
         </div>
		 <?php if ($type == 'image'){
			
			 $link = 'image/actualite/'.$lien;
			 ?>
		 
             <img class="img-thumbnail" src="<?= $link ?>" alt=""/>
             <div class="page-header" style="margin: 0 0 20px">
			    <h3 style="color:#000"> <?= $marque[2] ?></h3>
			 </div>
		 <?php 	}  ?>
			<?php  if($type == 'video')
			{ 
		echo false;
				$sousCh = "watch";
					//echo strpos($link,"watch");
					$link=str_replace("watch?v=","v/",$lien);
					
					?>
<div class="img-thumbnail">			
	<object  type="application/x-shockwave-flash" width="500" height="300" data="<?= $link ?>">
		<param name="movie" value="<?= $link ?>" />
		<param name="wmode" value="transparent" />
	</object>
<!--[if lte IE 6 ]>
	<embed src="http://www.youtube.com/v/_etwz7NkemE&hl=fr" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed>
<![endif]-->
</div>
<?php $lien=str_replace("v/","watch?v=",$link); ?>
 <div class="page-header">
			    <div class="row" style="margin-bottom:-10px;height=46px">
				<div class="col-lg-4" style="margin-top:-30px">
                  <h3 style="color:#000"> <?= $marque[2] ?></h3>
				</div>
				<div class="col-lg-8" style="margin-top:-10px">
				  <div  style="display:inline">
						<div class="fb-share-button" data-href="<?= $lien ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a style="margin-top:-15px" class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>
						
	
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?= $lien ?>" data-text="Sms Fm" data-lang="fr" data-dnt="true">Tweeter</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

						
	<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: fr_FR</script>
<script type="IN/Share" data-url="<?= $lien ?>" data-counter="right"></script>
  
   </div>
   </div>
			</div>			
             </div>
			<?php } ?>
			
			<div class="well">
			<p >
			<?= $marque[1] ?>
			
			</p>
            </div>
	</div>
	</div>
	</div>

	 <div class="col-lg-3" style="margin-left:-30px;">
	 <?php include('includes/bloc_presentation.html'); ?>
	</div>
	<div class="col-lg-3" style="margin-left:-30px;">
	 <div class="panel panel-primary" style="height:477px;">
		<div class="panel-heading">
			<h3 class="panel-title">Sondage</h3>
		</div>
		<div class="panel-body">

                            
<?php include 'includes/bloc_sondage.php'; ?>

		</div>
	</div>
	</div>
	
		   <div class="col-lg-3">
<div class="panel panel-primary" style="
    height:343px;
    margin-top: -362px;">
		<div class="panel-heading">
			<h3 class="panel-title">Météo</h3>
		</div>
		<div class="panel-body">
<div id="cont_fea7828270fcfcaba1da6262f6f6cb3c"><script type="text/javascript" async src="https://www.tameteo.com/wid_loader/fea7828270fcfcaba1da6262f6f6cb3c"></script></div>	
	    </div>
</div>
</div>
</div>
<?php include 'includes/footer.php' ?>
</div>
</div>

    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
	</body>
	</html>
