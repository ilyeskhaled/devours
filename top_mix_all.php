
  <head>
    <title>TOP Oriental</title>
    <link rel="stylesheet" type="text/css" href="resources_mx/css/main.min.css">
  </head>


  <body>
<?php 
include 'music_mx.php';
?>
     <div class='jAudio2'>

      <audio></audio>

      <div class='jAudio2--ui'>

        <div class='jAudio2--thumb'></div>

        <div class='jAudio2--status-bar'>

          <div class='jAudio2--details'></div>
          <div class='jAudio2--volume-bar'></div>

          <div class='jAudio2--progress-bar'>
            <div class='jAudio2--progress-bar-wrapper'>
              <div class='jAudio2--progress-bar-played'>
                <span class='jAudio2--progress-bar-pointer'></span>
              </div>
            </div>
          </div>

          <div class='jAudio2--time'>
            <span class='jAudio2--time-elapsed'>00:00</span>
            <span class='jAudio2--time-total'>00:00</span>
          </div>

        </div>

      </div>

       <div class='jAudio2--controls'>
        <ul>
          <li><button class='jAudio2--control jAudio2--control-prev' data-action='prev'><span></span></button></li>
          <li><button class='jAudio2--control jAudio2--control-play' data-action='play'><span></span></button></li>
          <li><button class='jAudio2--control jAudio2--control-next' data-action='next'><span></span></button></li>
        </ul>
      </div>
      <div class='jAudio2--playlist'>
      </div>

      

    </div>

    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <script src='resources_mx/js/jaudio.min.js'></script>
	
    <script type="text/javascript">var jOriental =<?php echo json_encode($mixes); ?>;</script>
    <script type="text/javascript" src="resources_mx/js/main.js"></script>
    <script type="text/javascript" src='resources_mx/js/main_all.js'></script>
	</body>
