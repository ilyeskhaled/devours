<?php 
if (isset($_POST['go']) && $_POST['go']=='ajouter actualité') 
{
    
	$media=$_POST['media'];
	
	$titre=$_POST['titre'];
	$description=$_POST['description'];
	$date=$_POST['date'];
	$photo="";
	if($media=="image"){
		$target_dir = "../image/actualite/";
		//echo $target_dir;
	    include 'upload.php' ;
		$photo=basename($_FILES["fileToUpload"]["name"]);
	}
	else
	{   
			$photo=$_POST['urlYoutube'];
		
	}
	//echo $photo;
	include 'connexionBd.php';
	
	// on insère notre actualité
	$sql = 'INSERT INTO actualite VALUES("","'.mysql_escape_string($titre).'","'.mysql_escape_string($description).'","'.mysql_escape_string($date).'","'.mysql_escape_string($photo).'","'.mysql_escape_string($media).'")';
	mysql_query('SET NAMES `utf8`');
	mysql_query($sql) or die('Erreur SQL !'.$sql.'<br />'.mysql_error());
	$validation = 'Merci votre actulité est ajouté avec succé ';
	
	//*/
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
   <head>
  <link rel="icon" href="icon_sms.png" type="image/x-icon">
<title>Insertion d'une nouvelle actualité</title>
 
	<script type="text/javascript">
	function valider ( )
{
    if ( document.formulaire.titre.value == "" )
    {
        alert ( "Veuillez entrer votre titre !" );
		formulaire.titre.focus();
        valid = false;
return valid;
    }
    if ( document.formulaire.description.value == "" )
    {
        alert ( "Veuillez entrer votre description !" );
		formulaire.description.focus();
        valid = false;
return valid;
    }
	if ( document.formulaire.date.value == "" )
    {
        alert ( "Veuillez entrer la date !" );
		formulaire.date.focus();
        valid = false;
return valid;
}
if (( !(document.getElementById('b1').checked)) && ( !(document.getElementById('b2').checked)))
    {
        alert ( "choisisez votre type de  media !" );
        valid = false;
        return valid;
}
if ((document.getElementById('b1').checked) && ( document.formulaire.fileToUpload.value == "" ))
    {
        alert ( "Veuillez entrer votre image !" );
		formulaire.fileToUpload.focus();
        valid = false;
return valid;
	}
	if ((document.getElementById('b2').checked) && ( document.formulaire.urlYoutube.value == "" ))
    {
        alert ( "Veuillez saisir url youtube !" );
		formulaire.urlYoutube.focus();
        valid = false;
return valid;
	}
	var chaine=document.formulaire.urlYoutube.value;
	var position=chaine.indexOf("https://www.youtube.com/");
	if (document.getElementById('b2').checked){
	if(position == -1){
		alert("url youtube non valide");
		formulaire.urlYoutube.focus();
		 valid = false;
return valid;
	}
	}
}
	function affiche_img()
{
	if(document.getElementById('b1').checked){
		document.getElementById('img').style.display="inline";
		document.getElementById('img').style.visibility="visible";
		document.getElementById('yut').style.visibility="hidden";
		document.getElementById('yut').style.display="none";
	}
	else 
	{
		document.getElementById('img').style.display="none";
		document.getElementById('img').style.visibility="hidden";
		document.getElementById('yut').style.display="inline";
		document.getElementById('yut').style.visibility="visible" ;
	}
}

</script>
 </head>
<body>

    <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ajout Actualité
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.php">Acceuil</a>
                            </li>
							<li>
							<i class="fa fa-fw fa-list"></i>
							<a href="list_actualite.php">&nbsp; List d'actualité</a>
							</li>
							<li class="active">
                                <i class="glyphicon glyphicon-plus"></i> Ajout actualité
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
				<?php
// on affiche les erreurs éventuelles
if (isset($validation)) {
	
?>
				 <div class="alert alert-success">
                    <strong>Se fait! </strong><?php echo " ". $validation ?>
                </div>
				<?php  
}				
// on affiche les erreurs éventuelles
if (isset($erreur)){
?>
                <div class="alert alert-danger">
                    <strong>Erreur</strong> <?= $erreur ?>
                </div>
				<?php
}
				?>
                    <div class="col-lg-6">
                        <form role="form" action="ajout_actualite.php" name="formulaire" method="post" enctype="multipart/form-data" onsubmit="return valider ();">
                            <div class="form-group">
                                <label>Titre d'actualité</label>
                                <input class="form-control" name="titre" >
                                <p class="help-block">Enrez le titre ici.</p>
                            </div>
							 <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" name="description" rows="3"></textarea>
                            </div>
							 <div class="form-group">
                                <label>Date</label>
                                <input type="date" class="form-control" name="date" >
                                <p class="help-block">Enrez la date ici.</p>
                            </div>
						    <div class="form-group">
                                <label>Type de media</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" id="b1" name="media" value="image" onchange="affiche_img();">Image
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" id="b2" name="media" value="video" onchange="affiche_img();">Video
                                    </label>
                                </div>
							</div>
							<div id="yut" class="yut" style="visibility: hidden; display: none">
							<div class="form-group">
                                <label>URL Youtube</label>
                                <input class="form-control" name="urlYoutube" >
                                <p class="help-block">Enrez URL ici.</p>
                            </div>
							</div>
							<div id="img" class="img" style="visibility: hidden; display: none">
                             <div class="form-group">
							  
                                <label>Choisir une image</label>
                                <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
					        </div>
                            </div>
							
                            <button type="submit" class="btn btn-default" value="ajouter actualité" name="go">Ajout actualité</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>



                    </div>
                   
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
