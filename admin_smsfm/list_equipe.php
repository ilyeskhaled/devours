<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<html>   <head>
<link rel="icon" href="icon_sms.png" type="image/x-icon">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">    
<title>List d'équipe</title>
 
</head>
<body>
  <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
<?php
include '../equipe.php';

?>
<div id="page-wrapper">
<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Equipe
                        </h1>
						
                        <ol class="breadcrumb">
                             <li>
                                <i class="fa fa-home"></i>  <a href="index.php">Acceuil</a>
                            </li>
							<li class="active">
							<i class="fa fa-users"></i>&nbsp; Liste d'équipe
							</li>
							<li>
                                <i class="fa fa-user-plus"></i>
								<a href="ajout_equipe.php">Ajout equipe</a>
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class=".col-lg-12">
                        <h2>List d'équipe</h2>

				  <?php  if (isset($_GET["msg"])) { 
 $msg = $_GET["msg"];
echo '<div class="alert alert-success"><strong> Merci! </strong>'.$msg.'</div>'; } ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>NOM</th>
                                        <th>Prenom</th>
                                        <th>info</th>
										<th>page fb</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
for($i=0;$i<count($equipe);$i++){
	$id=$equipe[$i]['id'];
?>
	<tr>
	<td><?= $equipe[$i]['nom']?></td>
	<td><?= $equipe[$i]['prenom']?></td>
	<td><?= $equipe[$i]['info']?></td>
	<td><?= $equipe[$i]['adressFb']?></td>
	<td> <a class="btn btn-warning" href="modifie_equipe.php?id=<?= $id ?>" onclick="return confirm('Etes vous sûre de vouloir modifier ce membre ?');">Modifie
	<a class="btn btn-danger" href="supprime_equipe.php?id=<?= $id ?>" onclick="return confirm('Etes vous sûre de vouloir supprimer ce membre ?');">Supprime
         </button>
	

	</td>
    </tr>	
	<?php
}
?>
</table>
</div>
</div>
</div>
  </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>