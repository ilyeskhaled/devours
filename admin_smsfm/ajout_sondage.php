<?php
// on teste si l'utilisateur a validé le formulaire et qu'il cherche à insérer le sondage dans la base, et non à ajouter une réponse au sondage
if (isset($_POST['go']) && $_POST['go']=='Valider') {
	if (!isset($_POST['question']) || empty($_POST['question'])) {
	$erreur = 'Votre question est vide.';
	}
	else {
	// on va regarder si l'utilisateur n'a pas laissé un champ vide
	$valid_form = 1;
	for ($i=1; $i<=$_POST['nb_reponses']; $i++){
		$temp = "reponse_$i";
		if (isset($_POST[$temp])) $value=$_POST[$temp];
		if (empty($value)) $valid_form = 0;
		echo "$value";
	}
	if ($valid_form == 0) {
		$erreur = 'Au moins une de vos réponse est vide.';
	}
	else {
		
		include 'connexionBd.php';

        $qst=$_POST['question'];
		
		// on insère notre question
		$sql = 'INSERT INTO sondage_questions VALUES("","'.mysql_escape_string($_POST['question']).'")';
		mysql_query($sql) or die('Erreur SQL !'.$sql.'<br />'.mysql_error());
       
		$id_sondage = mysql_insert_id($base);
		// on insère les réponses possibles à ce sondage
		for ($i=1; $i<=$_POST['nb_reponses']; $i++){
		$temp = "reponse_$i";
		if (isset($_POST[$temp])) $value=$_POST[$temp];
	
         
		$sql = 'INSERT INTO sondage_reponses VALUES("","'.$id_sondage.'","'.mysql_escape_string($value).'", "0")';
		$resutl = mysql_query($sql) or die('Erreur SQL !'.$sql.'<br />'.mysql_error());

		}
		// on redirige l'utilisateur à l'accueil du sondage
		if ($result){
		header("location: ajout_sondage.php");
		exit();
		}
	}
	}

}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<html>   <head>
<link rel="icon" href="icon_sms.png" type="image/x-icon">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">    
<title>Insertion d'un nouveau sondage</title>
 

</head>
<body>
  <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
<div id="page-wrapper">
<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Sondage
                        </h1>
                        <ol class="breadcrumb">
                             <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Acceuil</a>
                            </li>
							<li class="active">
							<i class="fa fa-percent"></i>&nbsp; Sondage
							</li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				 <div class="row">
                    <div class="col-lg-6">

                        <form role="form" action="ajout_sondage.php" method="post">
 <?php
if (isset($erreur)){
?>
                    <div class="alert alert-danger">
                    <strong>Erreur</strong> <?= $erreur ?>
                     </div>   
<?php
}
?>					 
						<div class="form-group">
                                <label>Question</label>
                                <input class="form-control" name="question" value="<?php if (isset($_POST['question'])) echo stripslashes(htmlentities(trim($_POST['question']))); ?>">
                                <p class="help-block">Entrez votre question ici.</p>
                            </div>

<?php
// on teste si la variable $_POST['nb_reponses'] est définie ou pas. Si elle ne l'est pas, on la défini à 1 (un sondage aura au moins une reéponse possible :)
if (!isset($_POST['nb_reponses'])) $_POST['nb_reponses'] = 1;

// si l'utilisateur a clické sur 'Ajouter une réponse' on incrémente la variable $_POST['nb_reponses'], ce qui va nous permettre de rajouter un champ de type text (pour la nouvelle réponse possible) à notre formulaire
if (isset($_POST['go']) && $_POST['go']=='Ajouter une réponse') $_POST['nb_reponses']++;

for ($i=1; $i<=$_POST['nb_reponses']; $i++){
	$temp = "reponse_$i";
	if (isset($_POST[$temp])) $value=$_POST[$temp];
	echo '<div class="form-group"><input class="form-control" name="reponse_'.$i.'" value="';
	if (isset($value)) echo stripslashes(htmlentities(trim($value)));
	echo '"></div>';
	unset($value);
}

                            
// on passe à notre formulaire le nombre de réponse au sondage
echo '<input type="hidden" name="nb_reponses" value="'.$_POST['nb_reponses'].'">';
echo '<button type="submit" class="btn btn-default" name="go" value="Ajouter une réponse">Ajouter une réponse</button>';
echo '<button type="submit" class="btn btn-default" name="go" value="Valider">Valider</button>';
echo '<button type="reset" class="btn btn-default">Reset Button</button>';

?>
</div>
   
</form>

              
                    <div class="col-lg-6">	
					<label>Resultat de sondage en cours</label>
					 <div class="panel panel-primary">
					<?php
                    include 'sondage_resultats.php';
					?>
					</div>
					</div>
				</div>
                <!-- /.row -->    

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
     <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>
</html>