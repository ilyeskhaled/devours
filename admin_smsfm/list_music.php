<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<html>   <head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="icon" href="icon_sms.png" type="image/x-icon">



<meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">    
<title>List de Music</title>
</head>
<body>
  <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
include '../music.php';
include '../music_mx.php';
include '../music_occ.php';

if (isset($validation)) echo '<br /><br />',$validation;
?>
<div id="page-wrapper">
<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Music
                        </h1>
                        <ol class="breadcrumb">
                             <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Acceuil</a>
                            </li>
							<li class="active">
							<i class="fa fa-music"></i>&nbsp; Liste Music
							</li>
							<li>
							
                                <i class="glyphicon glyphicon-plus"></i>
								<a href="ajout_music.php">Ajout Music</a>
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<div class="page-header">
                    <label>List de Music</label>
                </div>
                <div class="row">
				<div class="col-lg-12">
				<?php  if (isset($_GET["msg"])) { 
 $msg = $_GET["msg"];
echo '<div class="alert alert-success"><strong> Merci! </strong>'.$msg.'</div>'; } ?>
</div>
				<div class="col-lg-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <h3 class="panel-title">TOP Orientale</h3>
                            </div>
							<?php 
							$size=count($oriental);
if($size!=0){
	?>
                            <div class="panel-body">
							 <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Grade</th>
                                        <th>Artiste</th>
                                        <th>Titre</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
for($i=0;$i<count($oriental);$i++){
	$id=$oriental[$i]['id'];
?>
<tr>
<td><?= $oriental[$i]['grade'] ?></td>
<td><?= $oriental[$i]['nomArtiste'] ?></td>
<td><?= $oriental[$i]['titre']; ?> </td>
<td><a href="supprime_music.php?id=<?= $id ?>" onclick="return confirm('Etes vous s�re de vouloir modifier cette chanson ?');">Supprimer</a></td>
</tr>
<?php
}
?>
</table>
                            </div>
<?php 
}else {
	?>
	<div class="panel-body">
	<div class="alert alert-info">
                    <strong>Pas du music Oriental !</strong>
                </div>
	</div>
<?php
}
?>
                        </div>
                    </div>
					</div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
					<div class="panel panel-yellow">
                            <div class="panel-heading">
                                <h3 class="panel-title">TOP Occidental</h3>
                            </div>
                           <?php 
							$size=count($occidental);
if($size!=0){
	?>
                            <div class="panel-body">
							 <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Grade</th>
                                        <th>Artiste</th>
                                        <th>Titre</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
for($i=0;$i<count($occidental);$i++){
	$id=$occidental[$i]['id'];
?>
<tr>
<td><?= $occidental[$i]['grade'] ?></td>
<td><?= $occidental[$i]['nomArtiste'] ?></td>
<td><?= $occidental[$i]['titre']; ?> </td>
<td><a href="supprime_music.php?id=<?= $id ?>" onclick="return confirm('Etes vous s�re de vouloir modifier cette chanson ?');">Supprimer</a></td>
</tr>
<?php
}
?>
</table>
                            </div>
							</div>
<?php 
}else {
	?>
	<div class="panel-body">
	<div class="alert alert-info">
                    <strong>Pas du music Oriental !</strong>
                </div>
	</div>
<?php
}
?>
                        </div>
                    </div>
					
					 <!-- /.col-lg-6 -->
					 </div>
					 <!-- /.row -->
					 <div class="row">
					 
                    <div class="col-lg-6">
					<div class="panel panel-red">
                            <div class="panel-heading">
                                <h3 class="panel-title">TOP Mixes</h3>
                            </div>
                           <?php 
							$size=count($mixes);
if($size!=0){
	?>
                            <div class="panel-body">
							 <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Grade</th>
                                        <th>Artiste</th>
                                        <th>Titre</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
for($i=0;$i<count($mixes);$i++){
	$id=$mixes[$i]['id'];
?>
<tr>
<td><?= $mixes[$i]['grade'] ?></td>
<td><?= $mixes[$i]['nomArtiste'] ?></td>
<td><?= $mixes[$i]['titre']; ?> </td>
<td><a href="supprime_music.php?id=<?= $id ?>" onclick="return confirm('Etes vous s�re de vouloir modifier cette chanson ?');">Supprimer</a></td>
</tr>
<?php
}
?>
</table>
                            </div>
							</div>
<?php 
}else {
	?>
	<div class="panel-body">
	<div class="alert alert-info">
                    <strong>Pas du music Oriental !</strong>
                </div>
	</div>
<?php
}
?>
                       
                    </div>
					</div>
					
</div>
</div>

 <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>