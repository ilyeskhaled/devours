<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030"><head>
<link rel="icon" href="icon_sms.png" type="image/x-icon">


<title>List Podcast</title>
 
</head>
<body>
  <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
<?php
include 'podcast.php';
?>
<div id="page-wrapper">
<div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Podcast
                        </h1>
                        <ol class="breadcrumb">
                             <li>
                                <i class="fa fa-home"></i>  <a href="index.php">Acceuil</a>
                            </li>
							<li class="active">
							<i class="fa fa-microphone"></i>&nbsp; Liste podcast
							</li>
							<li>
                                 <i class="glyphicon glyphicon-plus"></i>
								<a href="ajout_podcast.php">Ajout podcast</a>
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class=".col-lg-12">
                        <h2>List podcast</h2>
<?php  if (isset($_GET["msg"])) { 
 $msg = $_GET["msg"];
echo '<div class="alert alert-success"><strong> Merci! </strong>'.$msg.'</div>'; } ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Titre</th>
                                        <th>pocast</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
for($i=0;$i<count($podcast);$i++){
	$id=$podcast[$i]['id'];
?>
	<tr>
	<td><?= $podcast[$i]['titre']?></td>
	<td><?= $podcast[$i]['url']?></td>
	<td> <a class="btn btn-warning" href="modifie_podcast.php?id=<?= $id ?>" onclick="return confirm('Etes vous sûre de vouloir modifier ce podcast ?');">Modifie</button>
	<a class="btn btn-danger" href="supprime_podcast.php?id=<?= $id ?>" onclick="return confirm('Etes vous sûre de vouloir supprimer ce podcast ?');" >Supprime
         
	

	</td>
    </tr>	
	<?php
}
?>
</table>


  <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>

</body>
</html>