<?php 
if (isset($_POST['go']) && $_POST['go']=='ajout music') 
{ 
	
	$target_dir = "";
	$target_dir2 = "";
	$type=$_POST['cat'];
	if($type=="Orientale"){
		
		$target_dir = "../resources/music/oriental/";
		$target_dir2 = "../resources/thumbs/";
	}
	else if($type=="Occidentale"){
		
		$target_dir = "../resources_occ/music/occidental/";
		$target_dir2 = "../resources_occ/thumbs/";
	}
	else
	{
		$target_dir = "../resources_mx/music1/mixes/";
		$target_dir2 = "../resources_mx/thumbs/";
	}
	//echo $target_dir;
	
	include 'uploadAudio.php' ;
	if($uploadOk == 0){
		$erreur = "Verifier la nom ou la taille de votre fichier";
	}
	else {
		$target_dir = $target_dir2 ;
		include 'upload.php' ;
		$photo=basename($_FILES["fileToUpload"]["name"]);
			
    include 'connexionBd.php';
	$titre=$_POST['titre'];
	$chanson=$_POST['chanson'];
	$grade=$_POST['grade'];
	$date=$_POST['date'];
	$url=basename($_FILES["fileToUploadMusic"]["name"]);
	// on insère notre membre
	$sql = 'INSERT INTO music VALUES("","'.mysql_escape_string($type).'","'.mysql_escape_string($titre).'","'.mysql_escape_string($chanson).'","'.mysql_escape_string($date).'","'.mysql_escape_string($url).'","'.mysql_escape_string($grade).'","'.mysql_escape_string($photo).'")';
	mysql_query('SET NAMES `utf8`');
	mysql_query($sql) or die('Erreur SQL !'.$sql.'<br />'.mysql_error());
	$validation = 'Merci votre music est bien ajouté ';
	
	
	
}
}	

?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
   <head>
   <link rel="icon" href="icon_sms.png" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">    
<title>Insertion d'une nouvelle chanson</title>

	
	<script type="text/javascript">

function valider ( )
{
    if ( document.formulaire.cat.value == "" )
    {
        alert ( "Veuillez choisir une categorie !" );
        valid = false;
       return valid;
    }
    if ( document.formulaire.titre.value == "" )
    {
        alert ( "Veuillez entrer le nom d'artiste !" );
		formulaire.titre.focus();
        valid = false;
return valid;
    }
	if ( document.formulaire.chanson.value == "" )
    {
        alert ( "Veuillez entrer le nom du chanson !" );
		formulaire.chanson.focus();
        valid = false;
return valid;
}
if ( document.formulaire.date.value == "" )
    {
        alert ( "Veuillez selectionner la date de chanson !" );
		formulaire.date.focus();
        valid = false;
return valid;
	}
if ( document.formulaire.fileToUpload.value == "" )
    {
        alert ( "Veuillez selectionner une image d'artiste !" );
		formulaire.fileToUpload.focus();
        valid = false;
return valid;
	}
if ( document.formulaire.fileToUploadMusic.value == "" )
    {
        alert ( "Veuillez selectionner la music !" );
		formulaire.fileToUploadMusic.focus();
        valid = false;
return valid;
	}
}
</script>
 </head>
<body>

    <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ajout Music
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.html">Acceuil</a>
                            </li>
							<li>
							<i class="fa fa-music"></i>
							<a href="list_music.php">List du Musique</a>
							</li>
							<li class="active">
                               <i class="glyphicon glyphicon-plus"></i> Ajout Music
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
				<?php
// on affiche les erreurs éventuelles
if (isset($validation)) {
	
?>
				 <div class="alert alert-success">
                    <strong>Se fait! </strong><?php echo " ". $validation ?>
                </div>
				<?php  
}				
// on affiche les erreurs éventuelles
if (isset($erreur)){
?>
                <div class="alert alert-danger">
                    <strong>Erreur</strong> <?= $erreur ?>
                </div>
				<?php
}
				?>
                    <div class="col-lg-6">
                        <form role="form" action="ajout_music.php" name="formulaire" method="post" enctype="multipart/form-data" onsubmit="return valider ();">
        
                            <div class="form-group">
							<div class="form-group">
                                <label>choisir l'emplacement de Music</label></br>
								<label></label>
								
                                <label class="radio-inline">
                                    <input type="radio" value="Orientale" name="cat">Oriental
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="Occidentale" name="cat">Occidental
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="Mixes" name="cat">Mixes
                                </label>
                            </div>
                                <label>Artiste</label>
                                <input class="form-control" name="titre" >
                                <p class="help-block">Enrez nom d'artiste ici.</p>
                            </div>
							<div class="form-group">
                                <label>Chanson</label>
                                <input class="form-control" name="chanson" >
                                <p class="help-block">Enrez le nom du chanson ici.</p>
                            </div>
							<div class="form-group">
							  
                                <label>Choisir une image</label>
                                <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
					        </div>
                            <div class="form-group">
                                <label>Date</label>
                                <input class="form-control" type="date" name="date" >
                                <p class="help-block">Enrez la date ici.</p>
                            </div>
                            <div class="form-group">
                                <label>Insere la chanson</label>
                                <input type="file"  name="fileToUploadMusic" id="fileToUploadMusic" accept="audio/*">
                            </div>
							<div class="form-group">
                                <label>Grade</label>
                                <select class="form-control" name="grade">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
									<option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
									
                                </select>
                            </div>
    

                            <button type="submit" class="btn btn-default" value="ajout music"  name="go">Ajout Music</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>



                    </div>
                   
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

   <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


</body>

</html>
