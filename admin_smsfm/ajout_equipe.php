<?php 
if (isset($_POST['go']) && $_POST['go']=='ajout Equipe') 
{
	$target_dir = "../image/equipe/";
	include 'upload.php' ;
	if($uploadOk == 0){
		$erreur = "Verifier la nom ou la taille de votre fichier";
	}
	else {
    include 'connexionBd.php';
	$nom=$_POST['nom'];
	$prenom=$_POST['prenom'];
	$info=$_POST['info'];
	$fb=$_POST['fb'];
	$photo=basename($_FILES["fileToUpload"]["name"]);
	//echo $nom . $prenom . $fb . $photo;
	// on insère notre membre
	$sql = 'INSERT INTO equipe VALUES("","'.mysql_escape_string($nom).'","'.mysql_escape_string($prenom).'","'.mysql_escape_string($info).'","'.mysql_escape_string($photo).'","'.mysql_escape_string($fb).'")';
	mysql_query('SET NAMES `utf8`');
	mysql_query($sql) or die('Erreur SQL !'.$sql.'<br />'.mysql_error());
	$validation = 'Merci votre membre est bien ajouté ';
	}
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >
<html>   <head>
<link rel="icon" href="icon_sms.png" type="image/x-icon">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge">  
 <meta name="viewport" content="width=device-width, initial-scale=1">    
<title>Insertion d'un nouveau membre</title>

<script type="text/javascript">

function valider ( )
{
    if ( document.formulaire.nom.value == "" )
    {
        alert ( "Veuillez entrer votre nom !" );
		formulaire.nom.focus();
        valid = false;
return valid;
    }
    if ( document.formulaire.prenom.value == "" )
    {
        alert ( "Veuillez entrer votre prenom !" );
		formulaire.prenom.focus();
        valid = false;
return valid;
    }
	if ( document.formulaire.info.value == "" )
    {
        alert ( "Veuillez entrer votre information !" );
		formulaire.info.focus();
        valid = false;
return valid;
}
if ( document.formulaire.fileToUpload.value == "" )
    {
        alert ( "Veuillez entrer votre photo !" );
		formulaire.fileToUpload.focus();
        valid = false;
return valid;
	}
if ( document.formulaire.fb.value == "" )
    {
        alert ( "Veuillez entrer votre url fb !" );
		formulaire.fb.focus();
        valid = false;
return valid;
}
var chaine=document.formulaire.fb.value;
	var position=chaine.indexOf("www.facebook.com/");
	if(position == -1){
		alert("url facebook non valide");
		formulaire.fb.focus();
		 valid = false;
return valid;
	}
}
</script>
 </head>
<body>

    <div id="wrapper">
 <?php 
   include 'includes/header.html';
   include 'includes/menu.html';
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ajout Membre Equipe
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>  <a href="index.php">Acceuil</a>
                            </li>
							<li>
							<i class="fa fa-users"></i>
							<a href="list_equipe.php">&nbsp; List d'équipe</a>
							</li>
							<li class="active">
                                <i class="fa fa-user-plus"></i> Ajout equipe
                            </li>
							<li>
							<i class="glyphicon glyphicon-globe"></i>
							<a href="https://www.smsradio.smsfm.tn/">Consultez Site</a>
							</li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
				<?php
// on affiche les erreurs éventuelles
if (isset($validation)) {
	
?>
				 <div class="alert alert-success">
                    <strong>Se fait! </strong><?php echo " ". $validation ?>
                </div>
				<?php  
}				
// on affiche les erreurs éventuelles
if (isset($erreur)){
?>
                <div class="alert alert-danger">
                    <strong>Erreur</strong> <?= $erreur ?>
                </div>
				<?php
}
				?>
                    <div class="col-lg-6">
                        <form role="form" action="ajout_equipe.php" name="formulaire" method="post" enctype="multipart/form-data" onsubmit="return valider ();">
                       

                            <div class="form-group">
                                <label>Nom</label>
                                <input class="form-control" name="nom" >
                                <p class="help-block">Enrez nom ici.</p>
                            </div>
							<div class="form-group">
                                <label>Prenom</label>
                                <input class="form-control" name="prenom" >
                                <p class="help-block">Enrez Prenom ici.</p>
                            </div>
                            <div class="form-group">
                                <label>Info de renseignement</label>
                                <input class="form-control" name="info" >
                                <p class="help-block">Enrez info ici.</p>
                            </div>
                            <div class="form-group">
                                <label>Choisir une photo</label>
                                <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
                            </div>
                            <div class="form-group">
                                <label>Page Fb</label>
                                <input class="form-control" name="fb" >
                                <p class="help-block">Enrez adress facebook.</p>
                            </div>
    

                            <button type="submit" class="btn btn-default" value="ajout Equipe" name="go">Ajout membre</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>



                    </div>
                   
                    </div>
               
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


</body>

</html>
