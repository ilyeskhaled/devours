-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 07, 2018 at 08:22 AM
-- Server version: 5.6.39-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smsfm_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `actualite`
--

CREATE TABLE `actualite` (
  `id` int(11) NOT NULL,
  `titre` varchar(128) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `date` date NOT NULL,
  `lien` varchar(255) CHARACTER SET utf8 NOT NULL,
  `type` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `actualite`
--

INSERT INTO `actualite` (`id`, `titre`, `description`, `date`, `lien`, `type`) VALUES
(30, 'SMS - Partenariat tuniso-allemand-dialogue des cultures', 'Réception chez Monsieur le Gouverneur de Mahdia #Najem #Gharsalli Programme et Objectifs • Informations et échanges sur les diff', '2014-05-04', 'https://www.youtube.com/watch?v=UpmoV6IMCM4', 'video'),
(31, 'Société SMS- INNOVATION-Présence Média', 'Société SMS, Centre de Formation et Société de Services. http://societesms.wordpress.com/ La Société SMS vous implique et vous f', '2014-05-04', 'https://www.youtube.com/watch?v=QtElsYhJ0w0', 'video'),
(32, 'Echange Culturel tuniso-allemand', 'Programme et Objectifs • Informations et échanges sur les différentes formes de travail national et international entre les jeun', '2015-02-08', 'https://www.youtube.com/watch?v=infzBK4ux_k', 'video'),
(33, 'Interview avec le Co-President Global Forum- directe démocratie Tunisie', 'Hamed Messaoud - Interview mit Bruno Kaufmann - Co-president Global Forum on Modern Direct Democraty - Editor-in-Chief, people2p', '2015-05-20', 'https://www.youtube.com/watch?v=0Dp8O6ARUaA', 'video'),
(34, 'Forum mondial sur la démocratie directe moderne Tunisie 2015', 'Hamed Messaoud ; Interview mit Michael von der Lohe ( Mehr Demokratie e.V. Germany) während der Globales Forum für moderne direk', '2015-05-20', 'https://www.youtube.com/watch?v=ZEWPj2c4zMA', 'video'),
(35, 'Mouvements citoyens et démocratie directe', 'Se Forum mondial sur la démocratie directe Hamed Messaoud, interview avec M. Klaus Hoffmann- Editor-in-Chief Direct democraty ( ', '2015-05-20', 'https://www.youtube.com/watch?v=un7zWjCGAR0', 'video'),
(36, 'Forum sur la démocratie directe moderne Tunisie 2015', 'Hamed Messaoud; Mon interview avec le professeur Rafael Pineiro - Professor for political Science at the University of Montevide', '2015-05-20', 'https://www.youtube.com/watch?v=8RiW7oK14g4', 'video'),
(37, 'Agile Life cycle - Scrum - Société SMS', 'agile life cycle ; whats agile ; ce quoi agile video presenté par societe SMS', '2013-08-14', 'https://www.youtube.com/watch?v=kLmuLupPRV4', 'video'),
(38, 'Soutenances des PFE chez la SMS Souassi - Mahdia 2012', 'Société SMS Mahdia - Souassi - Développement Technologique Société SMS - Séminaire - Thème: Présentation des projets de fin d\'ét', '2014-03-20', 'https://www.youtube.com/watch?v=nDCX0Kjm5YI', 'video'),
(39, 'Soutenances des PFE chez la SMS - Mahdia 2012', 'Société SMS Mahdia - Souassi - Développement Technologique Société SMS - Séminaire - Thème: Présentation des projets de fin d\'ét', '2014-03-21', 'https://www.youtube.com/watch?v=3IDv-2Oq9EA', 'video'),
(40, 'PEACE TUNISIA', 'Merci à tous les haut-parleurs, les mentors, la presse et les invités des événements merveilleux mercredi à Tunis Start Up House! Un projet conjoint de la START UP HAUS TUNIS (Enpact E.V Fondation Westerwelle) avec PEACETUNISIA. L\'événement a rassemblé les jeunes entrepreneurs tunisiens ont été en mesure de se connecter avec des experts tunisiens et allemands qui ont motivé et des conseils a donné pour plus de succès dans leur entreprise. Un merci spécial à la fondatrice de la maison, Sebastian et Matthias Rubatscher Tréutweîn, l\'équipe de direction Amel Ben Ali et Yassine Yousfi. ', '2016-10-07', 'peace.jpg', 'image'),
(41, 'الانقطاع المبكر عن الدراسة ', 'الانقطاع المبكر عن الدراسة : الاسباب ، النتائج و الحلول.\r\nموضوع مهمّ واكبه فريق SMSFM بمنطقة الشحيمات الشمالية #السواسي #اولاد_الشامخ #المهدية\r\nموضوع المائدة المسديرة التي نظمتها جمعية الشــمــال للمسرح والفنون يوم 16.02.2016 بمنطقة الشحيمات الشمالية احتضنتها المدرسة الاعدادية ', '2016-02-16', 'https://www.youtube.com/watch?v=EYYZPQ_lYjg', 'video'),
(44, 'حــوار مباشر مع والي المهدية من اجل التشغيل\r\n', 'أثناء زيارة الوالي لمعتمدية السواسي ... نقل حي ومباشر لــ SMSFM \r\nتحوّل والي المهدية السيد فوزي غراب صباح الثلاثاء مصحوبا بعديد الإطارات والمديرين الجهويين في زيارة كان الهدف منها التحاور المباشر مع المعتصمين طيلة اسبوعين ومحاولة إقناعهم بفك الإعتصام مقابل وعود وإلتزامات بتلبية بعض مطالب أصحاب الشهائد العليا والمعطلين عن العمل ', '2016-02-10', 'https://www.youtube.com/watch?v=vIyz41jXVHI', 'video'),
(45, 'Echanges et Partenariats-tuniso-allemand avec la Société SMS', 'Echange d\'expériences entre les Instituts tunisiens et les Instituts allemands.', '0000-00-00', '4.png', 'image'),
(46, 'Société SMS - Echanges et Partenariats - Tunisie-Allemagne', 'Informations et échanges sur les différentes formes de travail \r\nnational et international entre les jeunes des deux pays.', '0000-00-00', 'd.png', 'image'),
(47, 'Echanges et Partenariats', 'Nous voulons établir les Relation entre les Entrepreneurs ainsi que développer et lancer des Projets.', '0000-00-00', 'i.png', 'image'),
(48, 'Société SMS - Echanges et Partenariats - Tunisie-Allemagne', 'Informations et échanges sur les différentes formes de travail national et international entre les jeunes des deux pays.', '0000-00-00', '3.png', 'image'),
(50, 'Echanges et Partenariats-tuniso-allemand avec la Société SMS', 'Protection de l\'environnement - en termes de tourisme durable - Ecotourisme (pour les jeunes touristes allemands).', '0000-00-00', '5.png', 'image'),
(51, 'LA PHILOSOPHIE DE NOTRE ENTREPRISE', 'Les Relation et les liens entre l\'Europe et la Tunisie: \r\n1. Points forts \r\n2. Intensité \r\n3. Productivité\r\n4. De l\'avenir\r\nNous voulons établir les Relation entre les Entrepreneurs des Pays,développer et lancer des Projets.\r\nLa coopération, nous souhaitons développer une coopération dans les domaines suivants:\r\n1. L\'économie et le tourisme\r\n2. Le secteur financier\r\n3. l’industrie et la technique\r\n4. Les universités et l\'administration publique\r\nNous voulons être une interface de communication facilitant.', '2016-02-09', 'philo.jpg', 'image'),
(52, 'مواقع التواصل الإجتماعي تتفوق على المصادر التقليدية للأخبار', 'تطور وشيوع استعمال الإنترنت، منذ بداية القرن الحالي، لم تقتصر نتائجهما على ما توفر للإنسانية من فوائد جمة، تقنية واقتصادية وتعليمية واجتماعية، بل إنهما أعادا تشييد مفاهيم جديدة في الإعلام. وشيوع الإنترنت أفرز من جملة استتباعاته، بروز ضروب جديدة من وسائل الإعلام والوسائط، هي المواقع الاجتماعية أو وسائل التواصل الاجتماعي، وهي كثيرة ومتنوعة ومختلفة وذات توجهات ووسائل مختلفة.\r\n\r\nالمهم في ما نحن بصدده هو التساؤل عن آليات التواصل والتنافر بين وسائل الإعلام الجديدة (إن جاز لنا التعبير) وبين وسائل الإعلام التقليدية. التساؤل السابق يمكن ترجمته وتبسيطه بالبحث في مدى إمكانية اعتماد وسائل التواصل الاجتماعي كمصادر حقيقية وذات مصداقية للخبر؟ السؤال المركزي، منطلق بحثنا، يمكن تفريخه إلى العشرات من الأسئلة الفرعية المتصلة والمرتبطة بالسؤال المثير للجدل والسجال المثار أعلاه.\r\n\r\n\"الثورة\"التي أحدثتها مواقع التواصل الاجتماعي في مجال الإعلام، لم تحجب وجود انتقادات عميقة وكثيرة لمصداقية هذه الوسائل. جدير بالتذكير أن الثورات العربية، شهدت دعما كبيرا من وسائل التواصل الاجتماعي (فيسبوك وتويتر) والتي كانت ناقلا حيا ومباشرا لنبض الميادين والساحات. كانت الصور ومقاطع الفيديو تلهم الناس، وتسعف المحطات التلفزيونية في آن.\r\n\r\nالصور التي كانت ترد من مواقع فيسبوك، مثلا، ورغم افتقادها في غالب الأحيان للجودة والحرفية، كانت شواهد على الحراك الشعبي، وكانت تلتقط من هواة تصادف وجودهم مع اندلاع الأحداث، أو كانوا فاعلين فيها، ولم تر العديد من المحطات التلفزيونية الكبيرة ذائعة الصيت، ضيرا في نشر واعتماد \"الوثائق\"الحصرية المتداولة على مواقع التواصل الاجتماعي، مع إشارة مرافقة دائمة \"صور بالهاتف\" للاعتذار الضمني المبطن عن رداءة النوعية. المهم هنا أن المواقع الاجتماعية عوضت غياب المحطات التلفزيونية المحترفة، عن موقع الحدث، وهو غياب قد يكون ناتجا عن فجائية الحدث، أو عن رفض السلطة السماح للطواقم الإعلامية بالحلول في الموقع.\r\n\r\nمن هنا كانت \"صحافة القرب\"، ولكن هل أن \"حصرية\" المواد التي تذيعها المواقع الاجتماعية، من صور ومقاطع فيديو وتصريحات أو تسريبات أو غيرها، كافية لاعتبارها مصادر ذات مصداقية أو يمكن الاعتداد بها أو الركون المطمئن إلى صحتها؟\r\n\r\nجدير بالإشارة إلى أن مصادر الخبر المعتادة والتي يمكن الوثوق بها هي أساسا، برقيا وكالات الأنباء، والصحف المعروفة، ووسائل الإعلام الأخرى من قبيل التلفزيونات والإذاعات، فضلا عن البيانات الرسمية التي تصدرها الحكومات أو الأحزاب أو المنظمات. جدير بالتذكير أيضا إلى أن القاعدة الأساسية في إذاعة بي بي سي، مثلا، تقوم على التحقق من الخبر مرتين للتيقن من صحته ومصداقيته، تفاديا للتلاعب أو التسويق الإعلامي. فهل تتوفر هذه المعايير في المواقع الاجتماعية؟\r\n\r\nتبدو القضية في حقيقتها، بمثابة وقوع منتج الخبر أو مستهلكه، بين خيارين أحلاهما مر: إما المسارعة على اعتماد الحصرية والسرعة وهي أبعاد توفرها المواقع الاجتماعية، وهو خيار له مآلات خطيرة أيضا، حيث قد يؤدي إلى نشر أخبار زائفة وكاذبة ومتسرعة، وإما خيار ثان يقوم على وجوب التثبت من صحة الخبر، بمقارنته ومقارعته بأخبار أخرى أو وسائل أخرى، وهو كذلك خيار له استتباعاته إذ يعني تأخر نشر الخبر وتبديد حصريته وأهميته، ومن ثم إصابته بالتقادم.\r\n\r\nلكن التقدم الكبير الذي تحققه المواقع الاجتماعية يوميا، والذي تثبته الأرقام والاستطلاعات، يشي بأنه ثمة ضرب من التغاضي عن المعايير المهنية المحترفة للإعلام والخبر، وتسابق من أجل “استهلاك” مواد توفرها المواقع الاجتماعية، بغثها وسمينها. استطلاع للرأي أجراه معهد رويترز لدراسة الصحافة في بريطانيا، في شهر يونيو من العام 2016، توصل إلى أن أكثر من نصف مستخدمي مواقع التواصل الاجتماعي يعتمدونها كمصادر للخبر بل يرفضون دفع الأموال للمصادر التقليدية لقاء ذلك. الاستطلاع أكد أن فيسبوك هو الشبكة الأكثر أهمية وشيوعا بين المستخدمين، بنسبة 44 بالمئة.\r\n\r\nالمثير في القضية أن وسائل إعلام محترمة، من ناحية مصداقيتها وخبرتها وحجم طاقمها وانتشارها، أصبحت تعتمد في أخبارها على وسائل التواصل الاجتماعي، وأصبح من المألوف أن نقرأ، جملا من قبيل “تداولت وسائل التواصل الاجتماعي اليوم خبر…”، ألا يعني ذلك أننا إزاء تحول كبير يحدث في ميدان الإعلام؟', '2017-02-28', '_103055_f3.jpg', 'image'),
(53, 'Formation a distance? a votre avis?', 'Nous proposons des cours par correspondance option marketing ou communication (bac+3 et master).\r\nL\'intérêt de cette formation est qu\'elle permet à toutes personnes de suivre une formation, tout en concevant son emploi ou alors possibilité pour les étudiants d\'effectuer différents stage en temps plein sur l\'année.', '2017-04-04', 'forma assur.jpg', 'image'),
(54, 'Il n’y a pas de réussite faciles ni d’échecs définitifs»', '- Si tu ne trouves pas de but, cherche ta passion. Ta passion ne mènera droit au but.\r\n- Seul ceux qui tentent l’absurde peuvent réaliser l’impossible. – Albert Einstein', '2017-04-12', '2185-600x420.jpg', 'image'),
(55, 'رأي تلميذة بالمدرسة الاعدادية بالشحيمات الشمالية حول اسباب الانقطاع عن الدراسة في سنّ مبكّرة', 'رأي تلميذة بالمدرسة الاعدادية بالشحيمات الشمالية حول اسباب الانقطاع عن الدراسة في سنّ مبكّرة\r\n=======================\r\nكما نطلعكم متابعينا أنّ فريق #SMSFM يعمل على مونتاج ربرتاج كامل حول هذا الموضوع / الانقطاع المبكر عن الدراسة : الاسباب ، النتائج و الحلول.\r\n\r\n====================================\r\nدعـــــــوة للتواصل معنا على و شاركونا أفكاركم و مشاريعكم الثقافية والتكنولوجية\r\nhttp://smsfm.tn/', '2017-04-12', 'https://www.youtube.com/watch?v=EYYZPQ_lYjg', 'video'),
(56, 'Offres d\'emploi Formateur logiciel Alize', 'PROFIL REQUIS\r\nDe formation Ingénieur Génie Civil ou technicien confirmé, vous avez développé des connaissances dans le domaine de la route et de ses équipements (fascicules CCTG -DTU, calculs ALIZE et dimensionnement de structures de chaussées), vous disposez de connaissances de base en matière de marchés publics et CCAG travaux.', '2017-05-11', 'offre-d-emploil.jpg', 'image'),
(57, 'COORDINATEUR DE RESSOURCES HUMAINES', 'Profil recherché:\r\n\r\nLicence en Psychologie ou Direction d’entreprises est un plus.\r\nDeux ans minimum d’expérience professionnelle.\r\nParler couramment anglais et français.\r\nDisponibilité minimum de 12 mois.\r\nCapacités communicatives.\r\nConnaissances générales en informatique.\r\n \r\n\r\nConnaissances souhaitées:\r\n\r\nMaster ou diplôme supérieur en Gestion de RH.\r\nExpérience sur le terrain avec une autre ONG.\r\nConnaissance de l’arabe, portugais et/ou espagnol.\r\nExpérience justifiée en gestion de personnes et équipes.\r\nDes connaissances avancées de SAP sont un plus.', '2017-05-11', 'AAEAAQAAAAAAAAsuAAAAJDA1ZjQ1ZTdmLWNjZjYtNGJjYi05MTBjLTdlMzQ3ODljNTQ3Yg.jpg', 'image'),
(58, 'علماء آثار يكتشفون بقايا أقدم إنسان في التاريخ بالمغرب تاريخها يقارب 300 ألف سنة', 'أعلن في ندوة صحفية نظمها معهد ماكس بلانك لأنثروبولوجيا التطور بجامعة Leipzig الألمانية بالتعاون مع مؤسسة Collège de France للبحث العلمي و التعليم العالي، عن إكتشاف أقدم حفرية بشرية لإنسان Homo Sapiens أو الإنسان العاقل في منطقة جبل إيغود بنواحي مدينة اليوسفية، الأحفورة التي تم العثور عليها تم تأريخها لما بين 315 ألف و 300 ألف سنة، و هو ما يعاكس الطرح المتفق عليه منذ عدة سنوات، حول كون أقدم إنسان هومو سابيينس، يعود.تاريخه لحوالي 195 ألف سنة، و تم اكتشافه في منطقة شرق إفريقيا (إثيوبيا)، مما يعني أن المغرب اليوم يشكل المهد الجديد للبشرية.\r\nالإكتشاف تم بواسطة فريق بحث و تنقيب دولي برئاسة عالم الباليوأنثروبولوجيا الفرنسي Jean-Jacques Hublin، و منطقة البحث هي عبارة عن كهف قديم تم هدمه في إطار التنقيب المنجمي عن المعادن منذ ستينات القرن الماضي، و أولى الأحافير البشرية التي عثر عليها هناك تعود لعام 1962، لكن المنطقة لم تحظى بالإهتمام العلمي المطلوب حتى السنوات الاخيرة، حيث أعيد نبشها من جديد في إطار مشروع مولته جامعة Leipzig الألمانية، بالتعاون مع المعهد الوطني لعلوم الأركيولوجيا برئاسة البروفيسور عبد الواحد بناصر، و الحفريات المكتشفة هي عبارة عن بقايا هياكل عظمية لخمسة أشخاص من بينهم طفل بعمر 6 أو 7 سنوات، بالإضافة لأدوات حجرية من Silex تعود للعصر الحجري المتوسط.\r\nوأكد بلاغ للمعهد الوطني لعلوم الآثار والتراث أن فريقا دوليا، بإشراف كل من عبد الواحد بن نصر عن المعهد الوطني لعلوم الآثار والتراث التابع لوزارة الثقافة والاتصال، وجان جاك يوبلان، عن معهد ماكس بلانط للأنثروبولوجيا المتطورة في ألمانيا، قاما بإماطة اللثام عن بقايا عظام إنسان ينتمي لفصيلة الإنسان العاقل البدائي، مرفوقة بأدوات حجرية ومستحثات حيوانية بموقع جبل إيغود، في إقليم اليوسفية، جهة مراكش تانسيفت.\r\nوحسب علماء الآثار فإن تاريخها يرجع إلى ما يقرب من 300 ألف سنة.', '2017-06-09', 'https://www.youtube.com/watch?v=7Y9WD1t9Ams', 'video'),
(59, 'دعوة للمشاركة في ملء استمارة الاستبيان حول الرأي عن المرأة ودورها في المجتمع', 'تحية و تقديرا : \r\nتحمل هذه الاستمارة استييانا لبحث يتناول الرأي عن المراة ودورها في المجتمع', '2018-04-14', '581459_10151725003068917_247190202_n.jpg', 'image');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(30) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `login`, `pass`) VALUES
(4, 'boumerdes', 'boumerdes'),
(5, 'boumerdes1', 'boumerdes1');

-- --------------------------------------------------------

--
-- Table structure for table `balag`
--

CREATE TABLE `balag` (
  `id` int(11) NOT NULL,
  `msg` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balag`
--

INSERT INTO `balag` (`id`, `msg`, `file`) VALUES
(93, 'jjjj111', 'circu32.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `balagfr`
--

CREATE TABLE `balagfr` (
  `id` int(11) NOT NULL,
  `msg` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `balagfr`
--

INSERT INTO `balagfr` (`id`, `msg`, `file`) VALUES
(20, 'febee', 'CmdDos.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `connectes`
--

CREATE TABLE `connectes` (
  `ip` int(6) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `page` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `tel` int(11) NOT NULL,
  `msg` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`nom`, `prenom`, `email`, `tel`, `msg`) VALUES
('rtgrs', 'dsfsdg', 'dfds', 125, 'fsdgh'),
('sd', 'wxq', '12555', 0, 'csdfrgsr cvfgr'),
('ness', 'taher', '25846', 0, 'dbo,\r\n\r\n\r\n\r\ncorrdiallemene'),
('ness', 'taher', 'email@example.com', 25846, 'dbo,\r\n\r\n\r\n\r\ncorrdiallemene'),
('ahmed', 'abdelhamid', ' email@example.com', 215842, 'dfsgdfg dfsfgdt fdgdt dffgth dffg dfsgte fgtdy-'),
('nessrine', 'taher', 'email@example.com', 2543512, 'habsl qsja qsjaz zasf djerjet'),
('al', 'df', 'taher.nessrine@gmail.com', 0, ''),
('', '', '', 0, ''),
('', '', '', 0, ''),
('f', 'f', 'taher.nessrine@gmail.com', 0, 'bonsoir');

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `id` int(255) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(64) NOT NULL,
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `photo` varchar(128) NOT NULL,
  `adressFb` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`id`, `nom`, `prenom`, `info`, `photo`, `adressFb`) VALUES
(1, 'MESSAOUD', 'Hamed', 'Gérant d\'entreprise, à La Société Méditerranéenne ...', 'hamed.jpg', 'https://www.facebook.com/smstunisie/'),
(2, 'BRAEK', 'Haythem', 'A étudié à ESSTHSPromotion 2010', 'haythem.jpg', 'https://www.facebook.com/smstunisie/'),
(6, 'AB', 'Intisar', 'Assistante de Direction, à La Société Méditerranéenne de Services SMS', 'intisar.jpg', 'https://www.facebook.com/smstunisie/');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `lien` varchar(164) NOT NULL,
  `id_evnt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `lien`, `id_evnt`) VALUES
(1, '1.jpg', 0),
(2, '2.jpg', 0),
(3, '3.jpg', 0),
(4, '4.jpg', 0),
(5, '5.jpg', 0),
(6, '6.jpg', 0),
(7, '7.jpg', 0),
(8, '8.jpg', 0),
(9, '9.jpg', 0),
(10, '10.jpg', 0),
(11, '11.jpg', 0),
(12, '12.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `id` int(11) NOT NULL,
  `categorie` varchar(50) NOT NULL,
  `nomArtiste` varchar(50) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `url` varchar(100) NOT NULL,
  `grade` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`id`, `categorie`, `nomArtiste`, `titre`, `date`, `url`, `grade`, `image`) VALUES
(2, 'Orientale', 'Hamaki', 'ma balash ', '2016-10-09', 'Hamaki.mp3', 3, 'hamaki.jpg'),
(4, 'Occidentale', 'Justin bieber', 'cold water', '2016-10-23', 'Cold.mp3', 1, 'cold.jpg'),
(7, 'Occidentale', 'Rihanna', 'This Is What You Came For ', '0000-00-00', 'Rihanna.mp3', 2, 'Rihanna.jpg'),
(8, 'Occidentale', 'DELYNO', 'private love', '2016-11-15', 'privateLove.mp3', 3, 'delyon.jpg'),
(9, 'Orientale', 'Ragheb Alama', 'chuftik Tlakgbat', '2016-11-22', 'Ragheb.mp3', 4, 'ragheb.jpg'),
(10, 'Orientale', 'Nawel El Zoghbi', 'Am behki maa 7ali ', '2016-11-22', 'Nawel.mp3', 5, 'nawal.jpg'),
(11, 'Orientale', 'Najwa karam', 'Yekhreb Baytak', '2016-11-01', 'Najwa.mp3', 6, 'najwa.jpg'),
(12, 'Orientale', 'Saad Lmjarred', 'Machi Sahel', '2016-11-08', 'Saad.mp3', 7, 'saad.jpg'),
(13, 'Orientale', 'Carole Samaha', 'Hayda Adari ', '2016-11-13', 'Carole.mp3', 8, 'carole.jpg'),
(14, 'Orientale', 'Wael Jassar', 'Matez3alsh Meni ', '2016-11-15', 'Wael.mp3', 9, 'wael.jpg'),
(15, 'Orientale', 'Ramy Gamal ', 'Ewediny', '2016-11-14', 'ramy.mp3', 10, 'ramy.jpg'),
(16, 'Mixes', 'Dj Bach', 'Dj Bach', '2016-11-14', 'Dj Bach.mp3', 1, 'dj1.jpg'),
(17, 'Mixes', 'Dj Maru', 'Dj Maru', '0000-00-00', 'DJ Maru.mp3', 2, 'dj2.jpg'),
(18, 'Mixes', 'Dj Rayhan', 'Dj Rayhan', '2016-11-01', 'Dj Rayhan.mp3', 3, 'dj3.jpg'),
(19, 'Occidentale', 'Adele ', 'Send my love', '2016-12-06', 'Adele.mp3', 4, 'adele.jpg'),
(20, 'Occidentale', 'Mahmut Orhan.ft Sena Sener', 'feel', '2016-12-04', 'Mahmut Orhan.mp3', 5, 'feel.jpg'),
(21, 'Occidentale', 'Enrique Iglesias', 'DUELE EL CORAZON', '2016-12-04', 'Enrique Iglesias.mp3', 6, 'enrique.jpg'),
(22, 'Occidentale', 'Imany', 'Dont be so shy', '2016-12-05', 'Imany.mp3', 7, 'imany.jpg'),
(23, 'Occidentale', 'Calvin Harris', 'This is what you came for', '2016-12-07', 'Calvin Harris.mp3', 8, 'calvin.jpg'),
(24, 'Occidentale', 'SIa ft. sean paul', 'Cheap Thrills', '2016-12-04', 'sean paul.mp3', 9, 'sean.jpg'),
(25, 'Occidentale', 'Kungs vs Cookin’ on 3 Burners', 'This Girl', '2016-12-04', 'This Girl.mp3', 10, 'girl.jpg'),
(26, 'Mixes', 'Dj Danger', 'Dj danger', '2016-12-01', 'DJ DANGER.mp3', 4, 'dj4.jpg'),
(27, 'Mixes', 'DJ Ramzus', 'Ramzus', '2016-12-08', 'Ramzus.mp3', 5, 'dj5.jpg'),
(28, 'Mixes', 'DJ Raf', 'Dj raf', '2016-12-02', 'DJ Raf.mp3', 6, 'dj6.jpg'),
(31, 'Orientale', 'ellissa', 'Saharna Ya Lell', '2017-01-03', 'ellisa.mp3', 1, 'ellisa1.jpg'),
(32, 'Orientale', 'Asma ', 'Tamer el Hendi ', '2017-01-02', 'Asma.mp3', 2, 'asma.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `newsLettre`
--

CREATE TABLE `newsLettre` (
  `id` int(11) NOT NULL,
  `mail` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsLettre`
--

INSERT INTO `newsLettre` (`id`, `mail`) VALUES
(20, 'ilyeskhaled@hotmail.fr'),
(22, 'saddemsalah.contacts@gmail.com'),
(21, 'ilyeskhaled@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `podcast`
--

CREATE TABLE `podcast` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `podcast`
--

INSERT INTO `podcast` (`id`, `titre`, `url`) VALUES
(1, 'tuniso-allemand-dialogue des cultures', 'Partenariat tuniso-allemand-dialogue des cultures.mp3'),
(2, 'Présence Média', 'Présence Média 03.03.2014.mp3'),
(3, 'Partenariat tuniso-allemand-dialogue des cultures', 'Partenariat tuniso-allemand-dialogue des cultures.mp3'),
(4, 'Présence de Média', 'Présence Média 03.03.2014.mp3');

-- --------------------------------------------------------

--
-- Table structure for table `sondage_questions`
--

CREATE TABLE `sondage_questions` (
  `id` int(6) NOT NULL,
  `question` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sondage_questions`
--

INSERT INTO `sondage_questions` (`id`, `question`) VALUES
(3, 'Que pensez-vous de mon site');

-- --------------------------------------------------------

--
-- Table structure for table `sondage_reponses`
--

CREATE TABLE `sondage_reponses` (
  `id` int(6) NOT NULL,
  `id_sondage` int(6) NOT NULL,
  `reponse` varchar(100) NOT NULL,
  `nb_reponses` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sondage_reponses`
--

INSERT INTO `sondage_reponses` (`id`, `id_sondage`, `reponse`, `nb_reponses`) VALUES
(1, 3, 'sympa', 24),
(2, 3, 'po mal ', 22),
(3, 3, 'po terrible', 13),
(4, 3, 'je kiff :)', 42);

-- --------------------------------------------------------

--
-- Table structure for table `visites_jour`
--

CREATE TABLE `visites_jour` (
  `visites` mediumint(9) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visites_jour`
--

INSERT INTO `visites_jour` (`visites`, `date`) VALUES
(22, '2016-10-14'),
(12, '2017-01-03'),
(2, '2017-01-04'),
(6, '2017-01-05'),
(12, '2017-01-10'),
(12, '2017-01-20'),
(278, '2017-02-10'),
(188, '2017-02-11'),
(259, '2017-02-12'),
(150, '2017-02-13'),
(107, '2017-02-14'),
(63, '2017-02-15'),
(80, '2017-02-16'),
(74, '2017-02-17'),
(62, '2017-02-18'),
(96, '2017-02-19'),
(76, '2017-02-20'),
(47, '2017-02-21'),
(50, '2017-02-22'),
(184, '2017-02-23'),
(17, '2017-02-24'),
(35, '2017-02-25'),
(68, '2017-02-26'),
(63, '2017-02-27'),
(82, '2017-02-28'),
(40, '2017-03-01'),
(18, '2017-03-02'),
(41, '2017-03-03'),
(38, '2017-03-04'),
(38, '2017-03-04'),
(16, '2017-03-05'),
(36, '2017-03-06'),
(36, '2017-03-06'),
(55, '2017-03-07'),
(82, '2017-03-08'),
(108, '2017-03-09'),
(86, '2017-03-10'),
(97, '2017-03-11'),
(73, '2017-03-12'),
(92, '2017-03-13'),
(83, '2017-03-14'),
(99, '2017-03-15'),
(105, '2017-03-16'),
(106, '2017-03-17'),
(110, '2017-03-18'),
(117, '2017-03-19'),
(117, '2017-03-20'),
(116, '2017-03-21'),
(106, '2017-03-22'),
(105, '2017-03-23'),
(92, '2017-03-24'),
(91, '2017-03-25'),
(79, '2017-03-26'),
(108, '2017-03-27'),
(84, '2017-03-28'),
(77, '2017-03-29'),
(77, '2017-03-30'),
(76, '2017-03-31'),
(83, '2017-04-01'),
(12, '2017-04-02'),
(30, '2017-04-03'),
(93, '2017-04-04'),
(84, '2017-04-05'),
(89, '2017-04-06'),
(94, '2017-04-07'),
(79, '2017-04-08'),
(77, '2017-04-09'),
(78, '2017-04-10'),
(176, '2017-04-11'),
(125, '2017-04-12'),
(67, '2017-04-13'),
(60, '2017-04-14'),
(46, '2017-04-15'),
(72, '2017-04-16'),
(95, '2017-04-17'),
(83, '2017-04-18'),
(92, '2017-04-19'),
(94, '2017-04-20'),
(74, '2017-04-21'),
(64, '2017-04-22'),
(25, '2017-04-23'),
(20, '2017-04-24'),
(51, '2017-04-25'),
(49, '2017-04-26'),
(99, '2017-04-27'),
(94, '2017-04-28'),
(79, '2017-04-29'),
(88, '2017-04-30'),
(69, '2017-05-01'),
(56, '2017-05-02'),
(68, '2017-05-03'),
(36, '2017-05-04'),
(59, '2017-05-05'),
(74, '2017-05-06'),
(65, '2017-05-07'),
(26, '2017-05-08'),
(39, '2017-05-09'),
(38, '2017-05-10'),
(97, '2017-05-11'),
(29, '2017-05-12'),
(34, '2017-05-13'),
(23, '2017-05-14'),
(13, '2017-05-15'),
(41, '2017-05-16'),
(39, '2017-05-17'),
(40, '2017-05-18'),
(55, '2017-05-19'),
(66, '2017-05-20'),
(48, '2017-05-21'),
(26, '2017-05-22'),
(17, '2017-05-23'),
(41, '2017-05-24'),
(58, '2017-05-25'),
(47, '2017-05-26'),
(45, '2017-05-27'),
(34, '2017-05-28'),
(19, '2017-05-29'),
(27, '2017-05-30'),
(28, '2017-05-31'),
(35, '2017-06-01'),
(51, '2017-06-02'),
(182, '2017-06-03'),
(173, '2017-06-04'),
(27, '2017-06-05'),
(15, '2017-06-06'),
(18, '2017-06-07'),
(38, '2017-06-08'),
(36, '2017-06-09'),
(32, '2017-06-10'),
(34, '2017-06-11'),
(19, '2017-06-12'),
(68, '2017-06-13'),
(60, '2017-06-14'),
(15, '2017-06-15'),
(16, '2017-06-16'),
(19, '2017-06-17'),
(12, '2017-06-18'),
(14, '2017-06-19'),
(13, '2017-06-20'),
(53, '2017-06-21'),
(33, '2017-06-22'),
(13, '2017-06-23'),
(26, '2017-06-24'),
(16, '2017-06-25'),
(48, '2017-06-26'),
(112, '2017-06-27'),
(37, '2017-06-28'),
(21, '2017-06-29'),
(21, '2017-06-30'),
(39, '2017-07-01'),
(37, '2017-07-02'),
(53, '2017-07-03'),
(28, '2017-07-04'),
(10, '2017-07-05'),
(22, '2017-07-07'),
(20, '2017-07-08'),
(17, '2017-07-09'),
(54, '2017-07-10'),
(67, '2017-07-11'),
(43, '2017-07-12'),
(52, '2017-07-13'),
(34, '2017-07-14'),
(36, '2017-07-15'),
(33, '2017-07-16'),
(24, '2017-07-17'),
(32, '2017-07-18'),
(37, '2017-07-19'),
(21, '2017-07-20'),
(27, '2017-07-21'),
(42, '2017-07-22'),
(38, '2017-07-23'),
(16, '2017-07-24'),
(36, '2017-07-25'),
(30, '2017-07-26'),
(30, '2017-07-27'),
(38, '2017-07-28'),
(57, '2017-07-29'),
(53, '2017-07-30'),
(51, '2017-07-31'),
(59, '2017-08-01'),
(18, '2017-08-02'),
(38, '2017-08-03'),
(13, '2017-08-04'),
(23, '2017-08-05'),
(36, '2017-08-06'),
(37, '2017-08-07'),
(32, '2017-08-08'),
(29, '2017-08-09'),
(12, '2017-08-10'),
(10, '2017-08-11'),
(9, '2017-08-12'),
(22, '2017-08-13'),
(17, '2017-08-14'),
(14, '2017-08-15'),
(18, '2017-08-16'),
(14, '2017-08-17'),
(13, '2017-08-18'),
(14, '2017-08-19'),
(12, '2017-08-20'),
(23, '2017-08-21'),
(27, '2017-08-22'),
(43, '2017-08-23'),
(28, '2017-08-24'),
(19, '2017-08-25'),
(24, '2017-08-26'),
(29, '2017-08-27'),
(31, '2017-08-28'),
(49, '2017-08-29'),
(26, '2017-08-30'),
(49, '2017-08-31'),
(20, '2017-09-01'),
(35, '2017-09-02'),
(30, '2017-09-03'),
(22, '2017-09-04'),
(12, '2017-09-05'),
(29, '2017-09-06'),
(11, '2017-09-07'),
(15, '2017-09-08'),
(72, '2017-09-09'),
(49, '2017-09-10'),
(21, '2017-09-11'),
(14, '2017-09-12'),
(17, '2017-09-13'),
(13, '2017-09-14'),
(12, '2017-09-15'),
(34, '2017-09-16'),
(18, '2017-09-17'),
(22, '2017-09-18'),
(24, '2017-09-19'),
(17, '2017-09-20'),
(17, '2017-09-21'),
(13, '2017-09-22'),
(19, '2017-09-23'),
(14, '2017-09-24'),
(31, '2017-09-25'),
(10, '2017-09-26'),
(52, '2017-09-28'),
(1, '2017-09-29'),
(27, '2017-09-30'),
(27, '2017-10-01'),
(5, '2017-10-02'),
(94, '2017-11-01'),
(33, '2017-11-02'),
(35, '2017-11-03'),
(36, '2017-11-04'),
(4, '2017-11-05'),
(26, '2017-11-10'),
(38, '2017-11-11'),
(44, '2017-11-12'),
(79, '2017-11-13'),
(56, '2017-11-14'),
(32, '2017-11-15'),
(35, '2017-11-16'),
(46, '2017-11-17'),
(54, '2017-11-18'),
(55, '2017-11-19'),
(60, '2017-11-20'),
(63, '2017-11-21'),
(71, '2017-11-22'),
(43, '2017-11-23'),
(40, '2017-11-24'),
(51, '2017-11-25'),
(50, '2017-11-26'),
(50, '2017-11-27'),
(49, '2017-11-28'),
(98, '2017-11-29'),
(87, '2017-11-30'),
(78, '2017-12-01'),
(95, '2017-12-02'),
(76, '2017-12-03'),
(64, '2017-12-04'),
(73, '2017-12-05'),
(54, '2017-12-06'),
(40, '2017-12-07'),
(45, '2017-12-08'),
(15, '2017-12-09'),
(6, '2017-12-10'),
(11, '2017-12-11'),
(23, '2017-12-12'),
(15, '2017-12-13'),
(32, '2017-12-14'),
(22, '2017-12-15'),
(83, '2017-12-16'),
(3, '2017-12-17'),
(5, '2017-12-18'),
(14, '2017-12-19'),
(5, '2017-12-20'),
(7, '2017-12-21'),
(12, '2017-12-22'),
(9, '2017-12-23'),
(9, '2017-12-24'),
(5, '2017-12-25'),
(7, '2017-12-26'),
(7, '2017-12-27'),
(6, '2017-12-28'),
(30, '2017-12-29'),
(15, '2017-12-30'),
(6, '2017-12-31'),
(10, '2018-01-01'),
(1, '2018-01-02'),
(10, '2018-01-03'),
(5, '2018-01-04'),
(4, '2018-01-05'),
(10, '2018-01-06'),
(8, '2018-01-07'),
(11, '2018-01-08'),
(29, '2018-01-09'),
(7, '2018-01-10'),
(22, '2018-01-11'),
(57, '2018-01-12'),
(11, '2018-01-13'),
(6, '2018-01-14'),
(10, '2018-01-15'),
(4, '2018-01-16'),
(8, '2018-01-17'),
(4, '2018-01-18'),
(13, '2018-01-19'),
(9, '2018-01-20'),
(11, '2018-01-21'),
(15, '2018-01-22'),
(8, '2018-01-23'),
(2, '2018-01-24'),
(4, '2018-01-25'),
(24, '2018-01-26'),
(6, '2018-01-27'),
(23, '2018-01-28'),
(9, '2018-01-29'),
(14, '2018-01-30'),
(5, '2018-01-31'),
(11, '2018-02-01'),
(10, '2018-02-02'),
(5, '2018-02-03'),
(17, '2018-02-04'),
(13, '2018-02-05'),
(11, '2018-02-06'),
(6, '2018-02-07'),
(6, '2018-02-08'),
(5, '2018-02-09'),
(6, '2018-02-10'),
(10, '2018-02-11'),
(19, '2018-02-12'),
(12, '2018-02-13'),
(15, '2018-02-14'),
(14, '2018-02-15'),
(13, '2018-02-16'),
(12, '2018-02-17'),
(8, '2018-02-18'),
(16, '2018-02-19'),
(21, '2018-02-20'),
(4, '2018-02-21'),
(17, '2018-02-22'),
(18, '2018-02-23'),
(11, '2018-02-24'),
(8, '2018-02-25'),
(12, '2018-02-26'),
(46, '2018-02-27'),
(3, '2018-02-28'),
(3, '2018-02-28'),
(11, '2018-03-01'),
(21, '2018-03-02'),
(2, '2018-03-03'),
(5, '2018-03-04'),
(11, '2018-03-05'),
(7, '2018-03-06'),
(7, '2018-03-07'),
(9, '2018-03-08'),
(10, '2018-03-09'),
(15, '2018-03-10'),
(4, '2018-03-11'),
(43, '2018-03-12'),
(7, '2018-03-13'),
(20, '2018-03-14'),
(11, '2018-03-15'),
(76, '2018-03-16'),
(21, '2018-03-17'),
(2, '2018-03-18'),
(13, '2018-03-19'),
(11, '2018-03-20'),
(5, '2018-03-21'),
(10, '2018-03-22'),
(5, '2018-03-23'),
(11, '2018-03-24'),
(2, '2018-03-25'),
(8, '2018-03-26'),
(7, '2018-03-27'),
(9, '2018-03-28'),
(16, '2018-03-29'),
(11, '2018-03-30'),
(4, '2018-03-31'),
(11, '2018-04-01'),
(9, '2018-04-02'),
(32, '2018-04-03'),
(17, '2018-04-04'),
(33, '2018-04-05'),
(22, '2018-04-06'),
(23, '2018-04-07'),
(27, '2018-04-08'),
(39, '2018-04-09'),
(15, '2018-04-10'),
(38, '2018-04-11'),
(22, '2018-04-12'),
(23, '2018-04-13'),
(104, '2018-04-14'),
(17, '2018-04-15'),
(24, '2018-04-16'),
(30, '2018-04-17'),
(78, '2018-04-18'),
(49, '2018-04-19'),
(17, '2018-04-20'),
(29, '2018-04-21'),
(10, '2018-04-22'),
(20, '2018-04-23'),
(25, '2018-04-24'),
(17, '2018-04-25'),
(58, '2018-04-26'),
(7, '2018-04-27'),
(15, '2018-04-28'),
(6, '2018-04-29'),
(11, '2018-04-30'),
(13, '2018-05-01'),
(11, '2018-05-02'),
(18, '2018-05-03'),
(5, '2018-05-04'),
(3, '2018-05-05'),
(9, '2018-05-06'),
(16, '2018-05-07'),
(9, '2018-05-08'),
(6, '2018-05-09'),
(11, '2018-05-10'),
(8, '2018-05-11'),
(3, '2018-05-12'),
(1, '2018-05-16'),
(17, '2018-05-28'),
(9, '2018-05-29'),
(16, '2018-05-30'),
(24, '2018-05-31'),
(38, '2018-06-01'),
(11, '2018-06-02'),
(7, '2018-06-03'),
(11, '2018-06-04'),
(4, '2018-06-05'),
(7, '2018-06-06'),
(22, '2018-06-07'),
(9, '2018-06-08'),
(9, '2018-06-09'),
(7, '2018-06-10'),
(5, '2018-06-11'),
(6, '2018-06-12'),
(51, '2018-06-13'),
(49, '2018-06-14'),
(9, '2018-06-15'),
(37, '2018-06-16'),
(165, '2018-06-17'),
(74, '2018-06-18'),
(7, '2018-06-19'),
(15, '2018-06-20'),
(4, '2018-06-21'),
(14, '2018-06-22'),
(5, '2018-06-23'),
(20, '2018-06-24'),
(4, '2018-06-25'),
(8, '2018-06-26'),
(11, '2018-06-27'),
(15, '2018-06-28'),
(44, '2018-06-29'),
(16, '2018-06-30'),
(2, '2018-07-01'),
(22, '2018-07-05'),
(59, '2018-07-06'),
(20, '2018-07-07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actualite`
--
ALTER TABLE `actualite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `music`
--
ALTER TABLE `music`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsLettre`
--
ALTER TABLE `newsLettre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `podcast`
--
ALTER TABLE `podcast`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sondage_questions`
--
ALTER TABLE `sondage_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sondage_reponses`
--
ALTER TABLE `sondage_reponses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actualite`
--
ALTER TABLE `actualite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `music`
--
ALTER TABLE `music`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `newsLettre`
--
ALTER TABLE `newsLettre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `podcast`
--
ALTER TABLE `podcast`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sondage_questions`
--
ALTER TABLE `sondage_questions`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sondage_reponses`
--
ALTER TABLE `sondage_reponses`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
