<!DOCTYPE html>

<html>


   <head>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">  
	<link rel="icon" href="image/icon_sms.png" type="image/x-icon">  
<meta property="og:title" content="Le titre de ma page web" />
<meta property="og:description" content="La description de ma page web" />
<meta property="og:url" content="http://www.smsfm.tn/act.php" />
<meta property="og:image" content="http://www.smsfm.tn/icone/blog.png" />
      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Actualité</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

   

   </head>
   <body> 
   <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper">
	<div id="page-wrapper">

		<div class="container-fluid">
      

			<?php include('includes/headerIndex.php'); ?>

       
		
		
		</div>
 <div class="row">
  
	 <div class="col-lg-12" >
	 		<div class="panel panel-primary">
				
			<div class="panel-heading">
				<h3 class="panel-title">Actualité</h3>
			</div>
			<div class="panel-body">
                <?php include 'pagination.php' ; ?>

			</div>
</div>
</div>
</div>		
<?php require_once 'includes/footer.php' ?>
</div>
</div>

 <script src="js/jquery.js"></script>
      <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>