<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<link rel="icon" href="image/icon_sms.png" type="image/x-icon">
    <title>Equipe</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">

       
       
 <div id="page-wrapper">

            <div class="container-fluid">
             <?php include('includes/headerEq.php'); ?>
            </div>
<div class="row">
	
								
                                    

<div class="col-lg-12">

		<div class="panel panel-primary">
				
			<div class="panel-heading">
				<h3 class="panel-title">Equipe</h3>
			</div>
			<div class="panel-body">	
<?php
include 'equipe.php';
//echo  count($equipe);
for($i = 0; $i < count($equipe); $i++)
{
	$photo=$equipe[$i]['photo'];
	$url="image/equipe/". $photo;
    $nom=$equipe[$i]['nom'];
	$prenom=$equipe[$i]['prenom'];
	$info=$equipe[$i]['info'];
	$fb=$equipe[$i]['adressFb'];

	?>
          <div class="col-lg-6"style="margin-bottom:50px;">
                       <div class="panel-heading">
				           <h3 class="panel-title"><b style="
    color: #ff9800;
    font-size: 20px;
    font-family: cursive;"><?= $nom ?>&nbsp;<?= $prenom ?></b></h3>
						</div>        
				<div class="col-lg-4">
				<img class="img-thumbnail" height=100 src="<?= $url ?>"  alt=""/>
				</div> 
				<div class="col-lg-8">
	
                <div class="well" style="
    min-height: 20px;
">
				<p><?= $info ?> <br/> </p>
                </div>
				</div>


<a href="<?= $fb ?>" ><img src="image/fb.png"></a>
&nbsp;
<a href="https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fwww.fnmaker.com%2Ftwitterforpage%2Ftab%2F&ref_src=twsrc%5Etfw&screen_name=societesms&tw_p=followbutton"><img src="image/twit.png"></a>
					
							
			</div>


<?php 		
 } ?>
</div>
</div>	 
</div>
</div>

</div>
<?php include 'includes/footer.php' ?>

</div>
</div>

    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
	</body>
	</html>
