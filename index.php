<!DOCTYPE html>
<html>
<head>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">  
	  
<meta property="og:title" content="Le titre de ma page web" />
<meta property="og:description" content="La description de ma page web" />
<meta property="og:url" content="http://www.smsfm.tn/act.php" />
<meta property="og:image" content="http://www.smsfm.tn/icone/blog.png" />
<link rel="icon" href="image/icon_sms.png" type="image/x-icon">
      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Acceuil</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4755410254019877",
    enable_page_level_ads: true
  });
</script>
   </head>

   <body> 
   <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrapper">
	<div id="page-wrapper" style="margin-top:40px">

		<div class="container-fluid">
      

			<?php include('includes/header.php'); ?>

        </div>
		<div class="row" style="margin-top:-15px;margin-bottom:-15px">
		    <?php include('with-jquery.html'); ?>
		</div>
        <br/><br/>
        <div class="row">
  
	 <div class="col-lg-9" >
	 		<div class="panel panel-primary">
				
			<div class="panel-heading">
				<h3 class="panel-title">Actualité</h3>
			</div>
			<div class="panel-body">	
<?php include 'actualite.php';
for($i=0;$i<count($actualite);$i++){
	$id = $actualite[$i]['id'];
	$title = $actualite[$i]['titre'];
	$date = $actualite[$i]['date'];
	$description = $actualite[$i]['description'];
	$lien = $actualite[$i]['lien'];
	$type = $actualite[$i]['type'];
	$class = "#myModal".$id;
	$longeur = strlen($description); 
						if($longeur > 140){
							
							$description = substr($description, 0, 140);
							$pos = strrpos($description," ");
							$description = substr($description, 0, $pos);
							
						}
						

?>

				
			
				
                 
                      <div class="col-lg-6" style="min-height: 542.89px;">
					  <h4 style="color:#000;min-height:38px"><?= $title ?></h4>
					<?php 
					if($type=='image')
						{
							
							$link = 'image/actualite/'.$lien;
						
	?>
					  <img src="<?= $link ?>" alt="..." class="img-responsive" align="left" style="height:250px;"/> <br/><br/>
					  <div class="well" style="margin-top: 215px;">
						<p height="96.36px"><img src="image/date.png" width=15>&nbsp;&nbsp;<?= $date ?> <br/>
						<?= $description. " ..."  ?> <br/> </p>
						<a href="marque_actualite.php?id=<?= $id ?>" style="float: right;">
                               
                                    <span class="pull-left">Lire la suite &nbsp;</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                              
                        </a>
						</div>
						<?php }
						else {
							
							$sousCh = "watch";
					//echo strpos($link,"watch");
					$lien=str_replace("watch?v=","v/",$lien);
						?>
						<object type="application/x-shockwave-flash" width="100%" height="250" data="<?= $lien ?>">
		                    <param name="movie" value="<?= $lien ?>" />
		                    <param name="wmode" value="transparent" />
	                    </object>
						<div class="well" style="width:100%">
						<p style="96.36px"><img src="image/date.png" width=15 >&nbsp;&nbsp;<?= $date ?> <br/>
						
						<?= $description. " ..." ?>  <br/> 
					
						</p>
															
						<a href="marque_actualite.php?id=<?= $id ?>" style="float: right;">
						
                               
                                    <span class="pull-left">Lire la suite &nbsp;</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                               
                        </a>
						<?php
							$link=str_replace("v/","watch?v=",$lien);
						?>
						<br/>
						<br/>
						<div  style="display:inline">
						<div class="fb-share-button" data-href="<?= $link ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a style="margin-top:-15px" class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"></a></div>
						
	
							<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?= $link ?>" data-text="Sms Fm" data-lang="fr" data-dnt="true">Tweeter</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

						
	<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: fr_FR</script>
<script type="IN/Share" data-url="<?= $link ?>" data-counter="right"></script>
  
   </div>
						</div>
						<?php } ?>
						
                      </div>


			
 
<?php 

} ?>      

                   </div>
				    <p style="margin-left:75%;margin-bottom:20px;margin-top:-20px"><a href="page_actualite.php" class="btn btn-info" role="button">Voir plus &raquo;</a>
                    </p>
            </div>         
   </div>  
<div class="col-lg-3">                  
<?php include 'includes/bloc_presentation.html'; ?>
</div> 
<div class="col-lg-3">
<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Météo</h3>
		</div>
		<div class="panel-body" style="padding: 0px">
<div id="cont_fea7828270fcfcaba1da6262f6f6cb3c"><script type="text/javascript" async src="https://www.tameteo.com/wid_loader/fea7828270fcfcaba1da6262f6f6cb3c"></script></div>	
	    </div>
</div>
</div>

<div class="col-lg-3">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Podcast</h3>
		</div>
		<div class="panel-body">

                            
<?php include 'podcast.php'; ?>

		</div>
				<a href="#">
                                <div class="panel-footer">
                                    <span class="pull-left">Voir Details</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
        </a>
		
	</div>
</div>

<div class="col-lg-3">

	<div class="panel panel-primary"  style="min-height: 428px">
		<div class="panel-heading">
			<h3 class="panel-title">Sondage</h3>
		</div>
		<div class="panel-body">

                            
<?php include 'includes/bloc_sondage.php'; ?>

		</div>
	</div>
</div>

<div class="col-lg-3">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="panel-title">Newsletter</h3>
		</div>
		<div class="panel-body">

                            
<?php include 'includes/newsLetter.php'; ?>

		</div>
	</div>
</div>

  </div>
   <!-- Row  -->
   <div class="row">
 <div class="col-lg-12">
 <div class="panel panel-primary">
  
 <div class="panel-heading" >
 <h3 class="panel-title">TOP MUSIC</h3>
 </div>
<div class="panel-body">
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c;">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 100px;font-family: cursive;">TOP Oriental</h3>
		</div>
		</div>
		<?php require_once 'top_oriental.php' ?>
	 <p><a href="page_music.php" class="btn btn-primary" role="button">Voir plus &raquo;</a>
                    </p>
	</div>
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c;">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 25%;font-family: cursive;">TOP Occidental</h3>
	</div>
	</div>
        <?php require_once 'top_occidental.php' ?>
		 <p><a href="page_music.php" class="btn btn-primary" role="button">Voir plus &raquo;</a>
                    </p>
	</div>
	<div class="col-lg-4" style="margin-top:10px">
	<div class="panel panel-primary" style="border-color: #f55c5c">
	     <div class="panel-heading"  style="border-color: #f55c5c;background-color: #f55c5c;">
			<h3 class="panel-title"  style="font-size: 18px;padding-left: 100px;font-family: cursive;">TOP Occidental</h3>
		</div>
	</div>
        <?php require_once 'top_mix.php' ?>
		 <p><a href="page_music.php" class="btn btn-primary" role="button">Voir plus &raquo;</a>
                    </p>
    </div>

 </div>
 
 </div>   
   
</div>     
</div>  




<?php require_once 'includes/footer.php' ?>
</div>
</div>


 <script src="js/jquery.js"></script>
      <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>
</body>
</html>